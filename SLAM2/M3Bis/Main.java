import java.lang.reflect.Array;
import java.util.Scanner;
import java.util.ArrayList;
import java.util.Iterator;

import eu.hautil.mission3.*;
public class Main {

	public static void main(String[] args) {

		//ETAPE 1
		AdressePostale j = new AdressePostale("20 rue des","rutoto","agaga");
		j.afficher();
		System.out.println(j.getAdresse());
		System.out.println(j.getCP());
		System.out.println(j.getVille());
		j.setAdresse("zazaa");
		j.afficher();

		//ETAPE 2
		Bagage bagage1 = new Bagage(20,"rouge",15);
		bagage1.afficher();
		System.out.println(bagage1.getNumero());
		System.out.println(bagage1.getCouleur());
		System.out.println(bagage1.getPoids());
		bagage1.setCouleur("bleu");
		bagage1.afficher();

		//ETAPE 3
		Voyageur voyageur = new Voyageur("Valentin",20);
		voyageur.afficher();
		voyageur.setAdresse(j);
		voyageur.afficher();
		System.out.println(voyageur.getAdresse().getVille());
		System.out.println(voyageur.getAdresse().getAdresse());
		System.out.println(voyageur.getAdresse().getCP());
		System.out.println(voyageur.getNom());
		System.out.println(voyageur.getAge());
		voyageur.setNom("Bastien");
		voyageur.getAdresse().setVille("vovovo");
		voyageur.getAdresse().setCP("pouloulou");
		voyageur.afficher();

		//ETAPE 4
		voyageur.setBagage(bagage1);
		voyageur.afficher(); System.out.println(voyageur.getAdresse().getVille());
		System.out.println(voyageur.getAdresse().getAdresse());
		System.out.println(voyageur.getAdresse().getCP());
		System.out.println(voyageur.getAdresse().getVille());
		System.out.println(voyageur.getNom());
		System.out.println(voyageur.getAge());
		System.out.println(voyageur.getBagage().getCouleur());
		System.out.println(voyageur.getBagage().getPoids());
		System.out.println(voyageur.getBagage().getNumero());
		voyageur.setAge(5);
		voyageur.getBagage().setCouleur("rouge");
		voyageur.afficher();

		//ETAPE 5
		ArrayList <Voyageur> voyageurs = new ArrayList <Voyageur>();
		Voyageur v1 = new Voyageur("rouge",15);
		AdressePostale ad1 = new AdressePostale();
		v1.setAdresse(ad1);
		voyageurs.add(v1);
		Voyageur v2 = new Voyageur("bleu",16);
		AdressePostale ad2 = new AdressePostale();
		v2.setAdresse(ad2);
		voyageurs.add(v2);
		Voyageur v3 = new Voyageur("vert",16);
		AdressePostale ad3 = new AdressePostale();
		v3.setAdresse(ad3);
		voyageurs.add(v3);
		Voyageur v4 = new Voyageur("blanc",16);
		AdressePostale ad4 = new AdressePostale();
		v4.setAdresse(ad4);
		voyageurs.add(v4);
		Voyageur v5 = new Voyageur("noir",16);
		AdressePostale ad5 = new AdressePostale();
		v5.setAdresse(ad5);
		voyageurs.add(v5);
		for (Voyageur v : voyageurs) {
			v.afficher();
		}
		//ETAPE 6 (Recherche)
		Scanner sc = new Scanner(System.in);
		System.out.println("Entrez un nom de voyageur");
		String nom = sc.next();
		int flag=0;
		for (Voyageur v : voyageurs) {
			if(nom.equals(v.getNom())){
				v.afficher();
				flag = 1;
			}
		}
		if(flag==0) {
			System.out.println("Oups! Ca marche pas");
		}


		//ETAPE 7 (recherche + suppression)
		System.out.println("Entrez un nom de voyageur");
		String nomsuppr = sc.next();
		int flag2=0;
		Iterator<Voyageur> it = voyageurs.iterator(); // autre type de boucle plus safe pour la modification de la liste
		while(it.hasNext()) {
			Voyageur v = it.next();
			if(nomsuppr.equals(v.getNom())) {
				v.afficher();
				flag2=1;
				System.out.println("Voulez vous supprimez cet utilisateur? oui/non");
				String reponse = sc.next();
				while(reponse.equals("oui") || reponse.equals("non")) {
					if(reponse.equals("oui")) {
						it.remove();
						System.out.println("Voyageur supprimé : "+v.getNom());
						reponse = "ui";

					}
				}

			}

		}



		if (flag2==0) {
			System.out.println("Oups!Il y a une erreur");
		}
		else {
			for(Voyageur u : voyageurs) {
				u.afficher();
			}
		}
	}
}