package eu.siohautil.base;

public class Bagage {
    private int num;
    private String couleur;
    private double poids;


    public Bagage(int num, String coul, double poid){
        this.num = num;
        this.couleur = coul;
        this.poids = poids;
    }


    public void setNum(int num){
        this.num = num;
    }


    public int getNum(){
        return this.num;
    }


    public void setCouleur(String coul){
        this.couleur = coul;
    }

    public String getCouleur(){
        return this.couleur;
    }


    public void setPoid(float poid){
        this.poids = poid;
    }


    public double getPoid(){
        return this.poids;
    }


    public String toString(){
        return "Poids = "+this.poids+ " Couleur = " + this.couleur + " Numero = " + this.num;
    }
}
