package eu.siohautil.base;

public class AdressePostale {
    private String adresse;
    private String ville;
    private String codepostale;

    public AdressePostale(String adresse, String ville, String code ){
        this.adresse = adresse;
        this.ville = ville;
        this.codepostale = code;
    }
    public AdressePostale(){
        this.adresse = "20 rue des bernaches";
        this.ville = "Courdimanche";
        this.codepostale = "95800";
    }


    public void setAdresse(String adresse){
        this.adresse = adresse;
    }


    public String getAdresse(){
        return this.adresse;
    }


    public void setVille(String ville){
        this.ville = ville;
    }


    public String getVille() {
        return this.ville;
    }


    public void setCode(String code){
        this.codepostale = code;
    }


    public String getCode(){
        return this.codepostale;
    }


    public String toString(){
        return this.adresse + " " + this.ville + " " + this.codepostale;
    }

}
