package eu.siohautil.base;

import eu.siohautil.base.AdressePostale;

import java.util.ArrayList;

public class Agence {
    private String nom;
    private AdressePostale adresse;
    private ArrayList <Voyageur> listeVoy;


    public Agence(String nom,AdressePostale adresse){
        this.nom = nom;
        this.adresse = adresse ;
        listeVoy=new ArrayList<Voyageur>();
        Voyageur voy1 = new Voyageur(12,"titi");
        Voyageur voy2 = new Voyageur(16,"tata");
        Voyageur voy3 = new Voyageur(19,"toto");
        Voyageur voy4 = new Voyageur(32,"tutu");
        Voyageur voy5 = new Voyageur(14,"tete");
        listeVoy.add(voy1);
        listeVoy.add(voy4);        listeVoy.add(voy2);
        listeVoy.add(voy3);
        listeVoy.add(voy5);
    }


    public String getNom(){
        return this.nom;
    }
    public void setNom(String nom){
        this.nom=nom;

    }

    public AdressePostale getAdresse(){
        return this.adresse;
    }


    public void setAdresse(AdressePostale adresse){
        this.adresse = adresse;
    }


    public void addVoy(String nom,int age){
        Voyageur temp = new Voyageur(age,nom);
        listeVoy.add(temp);
    }

    public void rechercheNom(String cherche){
        int compt = 0;
        for(int i=0;i<listeVoy.size();i++){
            if(cherche.equals(listeVoy.get(i).getNom())){
                System.out.println(listeVoy.get(i).toString());
            }
            else{
                compt++;
            }
        }
        if(compt==listeVoy.size()){
            System.out.println("Ce nom n'est pas renseigné !");
        }

    }




    public void supr(String supr){
        int compt = 0;
        for(int i=0;i<this.listeVoy.size();i++){
            if(supr.equals(this.listeVoy.get(i).getNom())){
                this.listeVoy.remove(i);
            }
            else{
                compt++;
            }
        }
        if(compt==this.listeVoy.size()){
            System.out.println("Cet utilisateur n'existe pas");
        }

    }
    @Override
    public String toString() {
        return "Agence{" +
                "nom='" + nom + '\'' +
                ", adresse=" + adresse +
                ", listeVoy=" + listeVoy +
                '}';
    }





}
