package eu.siohautil.base;

import eu.siohautil.base.AdressePostale;
import eu.siohautil.base.Bagage;


public class Voyageur {
    protected int age;
    protected String nom;
    protected String categorie;
    protected AdressePostale adresse;
    private Bagage bagage;

    public Voyageur(int age, String nom){
        this.age = age;
        this.nom = nom;
        this.categorie = this.categorie();

    }
    public Voyageur(){
        this.age = 0;
        this.nom = " ";
    }




    public void setAge(int age){
        if(age>=0) {
            this.age = age;
            this.categorie = this.categorie();
        }
    }


    public String getNom(){
        return this.nom;
    }

    public void setNom(String nom){
        if(nom.length()>=2) this.nom = nom;
    }


    public AdressePostale getAdrsse(){
        return adresse;
    }


    public void setAdresse(AdressePostale adresse){
        this.adresse = adresse;
    }


    public void setBagage(Bagage bagage1){
        this.bagage= bagage1;
    }

    public Bagage getBagage() {
        return this.bagage;
    }
    private String categorie() {
        if (this.age < 2) {
            categorie = "Nourisson";
        } else {
            if (this.age < 12 && this.age >= 2) {
                categorie = "Enfant";
            } else {
                if (this.age >= 12 && this.age < 55) {
                    categorie = "Adulte";
                } else {
                    categorie = "Senior";
                }
            }
        }
        return categorie;
    }
    public String toString(){
        return "Nom =" +nom + "Age=" +age+"Categorie =" + categorie +"Adresse ="+ adresse.toString() + "Bagage =" + bagage.toString();
    }

}