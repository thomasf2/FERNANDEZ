package eu.siohautil.heritage;
import eu.siohautil.base.Voyageur;
public class VoyageurHandicape extends Voyageur {
    private String handicape;
     public VoyageurHandicape(int age, String nom, String handicape){
        super(age,nom);
        this.handicape = handicape;

    }

    public void setHandicape(String handi){
        this.handicape = handi;
    }
    public String getHandicape(){
        return handicape;
    }
    public String toString(){
        return super.toString()+"Handicape :"+handicape;
    }
}
