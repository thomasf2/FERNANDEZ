import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
public class JBDCSimple {
public static void main(String[] args) {
	try {
		Class.forName("org.mariadb.jdbc.Driver");
		System.out.println("driver ok");
		Connection conn = DriverManager.getConnection("jdbc:mariadb://sio-hautil.eu:3306/fernat?user=fernat&password=thomasf2");
		System.out.println("connection ok");
		Statement stmt = conn.createStatement();
		String req2 = "SELECT id,nom,specialite FROM bts_professeur";
		ResultSet result = stmt.executeQuery(req2);
		while (result.next()) {
			System.out.println("Id:" +result.getInt(1));
			System.out.println("Nom:" +result.getString(2));
			System.out.println("Specialité:"+result.getString(3));
		}
		result.close();
		stmt.close();
	}
	catch (Exception e) {
		e.printStackTrace();
	}

}

}
