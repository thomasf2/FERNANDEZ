import java.sql.Connection;
        import java.sql.DriverManager;
        import java.sql.PreparedStatement;
        import java.sql.ResultSet;
        import java.sql.Statement;
        import java.util.Scanner;
        import java.sql.*;

public class JDBCPrepare {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        try {
            Class.forName("org.mariadb.jdbc.Driver");
            System.out.println("driver ok");
            Connection conn = DriverManager.getConnection("jdbc:mariadb://sio-hautil.eu:3306/fernat?user=fernat&password=thomasf2");
            System.out.println("connection ok");
            System.out.println("Choisissez entre : Ajoutez un nouveau professeur [1] ; Supprimez un professeur [2] ; Recherchez une specialite d'un professeur ; Recherchez un nom d'un professeur [4]");
            int choix = sc.nextInt();
            if(choix ==1){
                System.out.println("Entrez un id");
                int id1= sc.nextInt();
                System.out.println("Entrez un nom");
                String nom1 = sc.next();
                System.out.println("Entrez une specialite");
                String spe1 = sc.next();
                String req1 = "INSERT INTO bts_professeur VALUES (?,?,?)";
                PreparedStatement ptstm = conn.prepareStatement(req1);
                ptstm.setInt(1,id1);
                ptstm.setString(2,nom1);
                ptstm.setString(3,spe1);
                int res1 = ptstm.executeUpdate();
            }
            else{

            }
            if(choix ==2){
                System.out.println("Entrez l'id du professeur à supprimer");
                int id2 = sc.nextInt();
                String req2 = "DELETE FROM bts_professeur WHERE id = ?";
                PreparedStatement ptstm2 = conn.prepareStatement(req2);
                ptstm2.setInt(1,id2);
                int res2 = ptstm2.executeUpdate();

            }
            else {

            }
            if (choix ==3){
                System.out.println("Entrez la spécialité recherchée");
                String spe2 =sc.next();
                String req3 = "SELECT * FROM bts_professeur WHERE specialite = ? ";
                PreparedStatement ptstm3 = conn.prepareStatement(req3);
                ptstm3.setString(1,spe2);
                ResultSet res3 = ptstm3.executeQuery();
                while(res3.next()){
                    System.out.println("nom :"+res3.getString(2));
                    System.out.println("specialite :"+res3.getString(3));
                }

            }
            else{

            }
            if (choix ==4){
                System.out.println("Entrez le nom recherché");
                String nom2 =sc.next();
                String req4 = "SELECT id,nom,specialite FROM bts_professeur WHERE nom = ? ";
                PreparedStatement ptstm4 = conn.prepareStatement(req4);
                ptstm4.setString(1,nom2);
                ResultSet res4 = ptstm4.executeQuery();
                while(res4.next()){
                    System.out.println("nom :"+res4.getString(2));
                    System.out.println("specialite :"+res4.getString(3));
                }
            }
            else{

            }

        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }
}
