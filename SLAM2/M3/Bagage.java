class Bagage {
	private int num;
	private String couleur;
	private int poids;
	

	public Bagage() {
		
	}
	public Bagage (int num , String couleur , int poids){
		this.num=num;
		this.couleur=couleur;
		this.poids=poids;
	}
	public void setNum(int num) {
		this.num=num;
	}
	public void setPoids(int poids) {
		this.poids=poids;
	}
	public void setCouleur(String couleur) {
		this.couleur=couleur;
	}
	public int getNum(int num) {
		return this.num;
	}
	public int getPoids(int poids) {
		return this.poids;
	}
	public String getCouleur(String couleur) {
		return this.couleur;
	}	
	@Override
	public String toString() {
		return "Bagage [num=" + num + ", couleur=" + couleur + ", poids=" + poids + "]";
	}
}
