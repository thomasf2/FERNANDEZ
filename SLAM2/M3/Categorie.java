
public enum Categorie {
	
	Nourrisson("Nourrisson"),
	Enfant("Enfant"),
	Ado("Ado"),
	Adulte("Adulte"),
	Senior("Senior");

	public String nom;

	private Categorie(String nom) {
		this.nom=nom;
	}
	
	
}
