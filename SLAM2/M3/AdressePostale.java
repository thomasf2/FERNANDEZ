class AdressePostale {
	private String ville;
	private String codepostal;
	private String libvoie;
	
	AdressePostale(String ville , String codepostal , String libvoie){
		this.ville = ville;
		this.codepostal = codepostal;
		this.libvoie = libvoie;
}
	AdressePostale(){
		this.ville = "Cergy" ;
		this.codepostal = "95800";
		this.libvoie = "20 rue des bernaches";
	}

	public void setVille(String ville) {
		this.ville = ville;
		
	}
	@Override
	public String toString() {
		return "AdressePostale [ville=" + ville + ", codepostal=" + codepostal + ", libvoie=" + libvoie + "]";
	}
	
	
}
	
