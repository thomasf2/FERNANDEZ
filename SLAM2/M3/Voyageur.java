import java.util.ArrayList;
//VOyageur = classe mere
public class Voyageur {
	protected String nom;
	protected int age;
	private int pose;
	//	private String categorie;
	private String ville;
	private String codepostal;
	private String libvoie;
	protected  String adresse;
	protected  Bagage bagage;
	protected Categorie categorie;
	private ArrayList<Voyageur> voyageur;

	/*private String categorie(int age){
		if (age < 3){
			categorie = "nourrisson";
		}

		else if (age >= 3 && age < 12){
			categorie = "enfant"; 
		}

		else if (age>=12 && age<18){
			categorie = "ado"; 
		}

		else if (age>=18 && age<55){
			categorie = "adulte"; 
		}

		else{
			categorie = "senior"; 
		}
		return categorie;

	}*/

	public Voyageur(String nom,int age){
		if ( age > 0) //pas de crochet car 1 instruction
			this.age = age;
		else
			this.age = 0;
		if (nom.length() >= 2)
			this.nom=nom;
		else 
			this.nom="fail";


		//	this.categorie(this.age);
		this.bagage=bagage;
		this.adresse=adresse;

		if (age < 3){
			this.categorie = categorie.Nourrisson;
		}

		else if (age >= 3 && age < 12){
			this.categorie = categorie.Enfant; 
		}

		else if (age>=12 && age<18){
			this.categorie = categorie.Ado; 
		}

		else if (age>=18 && age<55){
			this.categorie = categorie.Adulte; 
		}

		else{
			this.categorie = categorie.Senior; 
		}
	}
	public Voyageur(){
		age=0;
		nom="atata";
		this.categorie=categorie.Nourrisson;
	}	

	public Voyageur(String nom, String adresse) {
		super();
		this.nom = nom;
		this.adresse = adresse;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public int getPose() {
		return pose;
	}

	public void setPose(int pose) {
		this.pose = pose;
	}

	public String getVille() {
		return ville;
	}

	public void setVille(String ville) {
		this.ville = ville;
	}

	public String getCodepostal() {
		return codepostal;
	}

	public void setCodepostal(String codepostal) {
		this.codepostal = codepostal;
	}

	public String getLibvoie() {
		return libvoie;
	}

	public void setLibvoie(String libvoie) {
		this.libvoie = libvoie;
	}

	public String getAdresse() {
		return adresse;
	}

	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}

	public Bagage getBagage() {
		return bagage;
	}

	public void setBagage(Bagage bagage) {
		this.bagage = bagage;
	}

	public Categorie getCategorie() {
		return categorie;
	}

	public void setCategorie(Categorie categorie) {
		this.categorie = categorie;
	}

	@Override
	public String toString() {
		return "Voyageur [nom=" + nom + ", age=" + age + ", pose=" + pose + ", categorie=" + categorie + ", ville="
				+ ville + ", codepostal=" + codepostal + ", libvoie=" + libvoie + ", adresse=" + adresse + ", bagage="
				+ bagage + "]";
	}




}



