import java.awt.List;
import java.util.ArrayList;
import java.util.Scanner;
public class AgenceVoyage{
	private String nom;
	private AdressePostale adresse;
	private ArrayList<Voyageur> voylist;
	private Bagage bagage;
	Scanner sc = new Scanner (System.in);
	public AgenceVoyage () {
		this.nom = nom;
		this.adresse = adresse;
		this.voylist = new ArrayList<Voyageur>();
		Voyageur v1 = new Voyageur("Tata","20 rue de la tourelle");
		Voyageur v2 = new Voyageur("Titi","15 rue du dudu");
		Voyageur v3 = new Voyageur("Tota","14 rue du chezpas");
		Voyageur v4 = new Voyageur("tutu",18);
		voylist.add(new Voyageur("Toto","10 ru du"));
		voylist.add(v1);
		voylist.add(v2);
		voylist.add(v3);
		voylist.add(v4);
	}
	public AgenceVoyage(String nom, AdressePostale adresse, ArrayList<Voyageur> voylist) {
		super();
		this.nom = nom;
		this.adresse = adresse;
		this.voylist = voylist;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public AdressePostale getAdressePostale() {
		return adresse;
	}
	public void setAdressePostale(AdressePostale adresse) {
		this.adresse = adresse;
	}
	public void ajouter(Voyageur v) {
		voylist.add(v);
	}
	public Bagage getBagage() {
		return bagage;
	}
	public void setBagage(Bagage bagage) {
		this.bagage = bagage;
	}
	public void delete(String nom) {
		for (Voyageur e : voylist) {
			if(nom.equals(e.getNom())) {
				System.out.println("Voulez vous le supprimer? OUI/NON");
				String test = sc.next(); 
				if (test.equals("oui")) {
					voylist.remove(e);
				}
			}
		}
	}
	public void add(String nom) {
		for(Voyageur e : voylist)  {
			if(nom.equals(e.getNom())) {
				voylist.add(e);
			}
		}
	}
	public void cherche(String nom) {
		for(Voyageur v : voylist)  {
			if(nom.equals(v.getNom())) {
				v.toString();
			}		
		}
	}
	@Override
	public String toString() {
		return "AgenceVoyage [nom=" + nom + ", adresse=" + adresse + ", voylist=" + voylist + "]";
	}
}

