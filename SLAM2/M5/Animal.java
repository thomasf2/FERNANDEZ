
public class Animal {
	String surnom;
	int age;
	String type;
	public Animal() {
	}
	public Animal(String surnom, int age, String type) {
		super();
		this.surnom = surnom;
		this.age = age;
		this.type = type;
	}
	public void afficher(){
		System.out.println("Surnom :"+surnom+"Age :" + age+"Type :" + type);
	}
	public String getSurnom() {
		return surnom;
	}
	public void setSurnom(String surnom) {
		this.surnom = surnom;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
}
