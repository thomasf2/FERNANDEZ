


public abstract class Vehicule {
	protected String marque;
	protected String couleur;
	protected String modele;
	public Vehicule(String marque,String modele,String couleur){
		this.marque=marque;
		this.modele=modele;
		this.couleur=couleur;
	}
	void freiner() {
		System.out.println("je freine le vehicule");
	}
	void demarrer(){
		System.out.println("je demarre le vehicule");
	}
	public String getMarque() {
		return marque;
	}
	public void setMarque(String marque) {
		this.marque = marque;
	}
	public String getCouleur() {
		return couleur;
	}
	public void setCouleur(String couleur) {
		this.couleur = couleur;
	}
	public String getModele() {
		return modele;
	}
	public void setModele(String modele) {
		this.modele = modele;
	}
	

	
}