
public class Canard extends Animal implements Navigable {
	int nbPlumes;
	public Canard(int nbPlumes,String surnom,String type,int age){
		this.nbPlumes=nbPlumes;
		this.surnom=surnom;
		this.age=age;
		this.type=type;
	}
	public void accoster(){
		System.out.println("jaccoste le canard");
	}
	public void naviguer(){
		System.out.println("je navigue le canard");
	}
	public void couler(){
		System.out.println("je coule le canard");
	}
	
}
