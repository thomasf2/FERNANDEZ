-- Adminer 4.6.3 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `Equipe`;
CREATE TABLE `Equipe` (
  `id` int(11) NOT NULL,
  `idjoueur` int(11) NOT NULL,
  `club` varchar(60) NOT NULL,
  `ratiowin` varchar(60) NOT NULL,
  `logo` varchar(1000) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idjoueur` (`idjoueur`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `Equipe` (`id`, `idjoueur`, `club`, `ratiowin`, `logo`) VALUES
(1,	10,	'Fnatic',	'64%',	'https://upload.wikimedia.org/wikipedia/fr/thumb/3/36/Fnatic_Logo.svg/1200px-Fnatic_Logo.svg.png'),
(2,	11,	'Vitality',	'47%',	'https://www.armateam.org/wp-content/uploads/2018/02/team-vitality-logo-hstl.png'),
(3,	12,	'Schalke 04',	'50%',	'https://upload.wikimedia.org/wikipedia/commons/thumb/9/97/FC_Schalke_04_Logo.png/1200px-FC_Schalke_04_Logo.png'),
(4,	13,	'G2 Esport',	'60%',	'https://g2e-gamers2mediasl.netdna-ssl.com/wp-content/themes/g2-esports/library/img/download-logo-new-inverted.png'),
(5,	14,	'Misfits Gaming',	'54%',	'https://d1u5p3l4wpay3k.cloudfront.net/lolesports_gamepedia_en/thumb/7/73/Misfits_logo.png/220px-Misfits_logo.png?version=1f8265bca8189ba14647462a627976eb'),
(6,	15,	'Splyce',	'47%',	'https://gamewave.fr/static/images/esport/teams/304ac-Splycelogo_square.png'),
(7,	16,	'Unicorns of Love',	'48%',	'https://d1u5p3l4wpay3k.cloudfront.net/lolesports_gamepedia_en/thumb/a/a9/Unicorns_Of_Lovelogo_square.png/220px-Unicorns_Of_Lovelogo_square.png?version=79eecb01ca7a4c869dec2e23001af164'),
(8,	17,	'Roccat',	'40%',	'https://i1.wp.com/www.my-esport.fr/wp-content/uploads/2015/03/logo-roccat-2.png'),
(9,	18,	'Giants Gaming',	'38%',	'https://gamewave.fr/static/images/esport/teams/f9d9d-Giants_Gaminglogo_square.png'),
(10,	19,	'H2K',	'56%',	'https://d1u5p3l4wpay3k.cloudfront.net/lolesports_gamepedia_en/thumb/5/57/H2k-Gaminglogo_square.png/220px-H2k-Gaminglogo_square.png?version=00ced9a88d5b5ee452b6b75a2f02f377');

DROP TABLE IF EXISTS `Joueur`;
CREATE TABLE `Joueur` (
  `id` int(11) NOT NULL,
  `idequipe` int(11) NOT NULL,
  `poste` varchar(60) NOT NULL,
  `pseudo` varchar(60) NOT NULL,
  `ratio` double NOT NULL,
  `nom` varchar(60) NOT NULL,
  `age` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idequipe` (`idequipe`),
  CONSTRAINT `Joueur_ibfk_1` FOREIGN KEY (`idequipe`) REFERENCES `Equipe` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `Joueur` (`id`, `idequipe`, `poste`, `pseudo`, `ratio`, `nom`, `age`) VALUES
(1,	1,	'ADC',	'Rekkles',	5.8,	'Martin Larsson',	22),
(2,	1,	'TOPLANE',	'sOAZ',	3.6,	'Paul Boyer',	25),
(3,	1,	'JUNGLE',	'Broxah',	9.8,	'Mads Brock-Pedersen',	21),
(4,	1,	'MIDLANE',	'Caps',	3.7,	'Rasmus Winther',	19),
(5,	1,	'SUPPORT',	'Hylissang',	3.1,	'Zdravets Iliev Galabov',	23),
(6,	2,	'ADC',	'Attila',	5.5,	'Amadeu Carvalho',	22),
(7,	2,	'TOPLANE',	'Cabochard',	4.4,	'Lucas Simon-Meslet',	21),
(8,	2,	'JUNGLE',	'Gilius',	3.5,	'Erberk Demir',	22),
(9,	2,	'MIDLANE',	'Jiizuké',	3.4,	'Daniele di Mauro',	22),
(10,	2,	'SUPPORT',	'Jactroll',	3.9,	'Jakub Skurzynski',	20),
(11,	3,	'ADC',	'Upset',	6.7,	'Elias Lipp',	19),
(12,	3,	'TOPLANE',	'Vizicsacsi',	6.7,	'Tamás Kiss',	25),
(13,	3,	'JUNGLE',	'Amazing',	3.4,	'Maurice Stuckenschneider',	24),
(14,	3,	'MIDLANE',	'Nukeduck',	3.3,	'Erlend Vatevik Holm',	22),
(15,	3,	'SUPPORT',	'Vander',	5.4,	'Oskar Bogdan',	24),
(16,	4,	'ADC',	'Hjarnan',	5,	'Petter Freyschuss',	21),
(17,	4,	'TOPLANE',	'Wunder',	3.2,	'Martin Hansen',	20),
(18,	4,	'Jungle',	'Jankos',	4.6,	'Marcin Jankowski',	23),
(19,	4,	'MIDLANE',	'Perkz',	5.7,	'Luka Perkovic',	20),
(20,	4,	'SUPPORT',	'Wadid',	4,	'Kim Bae-in',	21),
(21,	5,	'ADC',	'Hans Sama',	4,	'Steven Liv',	21),
(22,	5,	'TOPLANE',	'Alphari',	5.7,	'Barney Morris',	19),
(23,	5,	'Jungle',	'Maxlore',	4.1,	'Nubar Sarafian',	22),
(24,	5,	'MIDLANE',	'Sencux',	6,	'Chres Laursen',	20),
(25,	5,	'SUPPORT',	'Mikyx',	4,	'Mihael Mehle',	20),
(26,	6,	'ADC',	'Kobbe',	5.7,	'Kasper Kobberup',	22),
(27,	6,	'TOPLANE',	'Odoamne',	2.4,	'Andrei Pascu',	21),
(28,	6,	'JUNGLE',	'Xerxe',	4.3,	'Andrei Dragomir',	19),
(29,	6,	'MIDLANE',	'Nisqy',	4.4,	'Yasin Dincer',	20),
(30,	6,	'SUPPORT',	'KaSing',	3.4,	'Raymond Tsang',	25),
(31,	7,	'ADC',	'Neon',	2.5,	'Matus Jakubcík',	21),
(32,	7,	'TOPLANE',	'WhiteKnight',	2.3,	'Matti Sormunen',	22),
(33,	7,	'JUNGLE',	'Kold',	3,	'Jonas Andersen',	23),
(34,	7,	'MIDLANE',	'Exileh',	1.9,	'Fabian Schubert',	22),
(35,	7,	'SUPPORT',	'Totoro',	1.7,	'Eun Jong-seop',	22),
(36,	8,	'ADC',	'HeaQ',	4.9,	'Martin Kordmaa',	21),
(37,	8,	'TOPLANER',	'Profit',	3.5,	'Kim Jun-hyung',	23),
(38,	8,	'JUNGLE',	'Memento',	3,	'Jonas Elmarghichi',	23),
(39,	8,	'MIDLANE',	'Blanc',	3.3,	'Jin Seong-min',	21),
(40,	8,	'SUPPORT',	'Norskeren',	2.5,	'Jin Seong-min',	19),
(41,	9,	'ADC',	'Steelback',	3.4,	'Pierre Medjaldi',	22),
(42,	9,	'TOPLANE',	'Ruin',	1.5,	'Kim Hyeong-min',	23),
(43,	9,	'JUNGLE',	'Djoko',	2.3,	'Charly Guillard',	21),
(44,	9,	'MIDLANE',	'Betsy',	1.9,	'Felix Edling',	21),
(45,	9,	'SUPPORT',	'SirNukesALot',	1.7,	'Risto Luuri',	22),
(46,	10,	'ADC',	'Sheriff',	1.6,	'Patrik Jiru',	18),
(47,	10,	'TOPLANE',	'SmittyJ',	1.2,	'Lennart Warkus',	22),
(48,	10,	'JUNGLE',	'Shook',	1.4,	'Ilyas Hartsema',	24),
(49,	10,	'MIDLANE',	'Selfie',	2.1,	'Marcin Wolski',	21),
(50,	10,	'SUPPORT',	'PromisQ',	1.4,	'Hampus Mikael Abrahamsson',	24);

DROP TABLE IF EXISTS `Match`;
CREATE TABLE `Match` (
  `id` int(11) NOT NULL,
  `idequipe1` int(11) NOT NULL,
  `idequipe2` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idequipe` (`idequipe1`),
  KEY `idequipe2` (`idequipe2`),
  CONSTRAINT `Match_ibfk_1` FOREIGN KEY (`idequipe1`) REFERENCES `Equipe` (`id`),
  CONSTRAINT `Match_ibfk_2` FOREIGN KEY (`idequipe2`) REFERENCES `Equipe` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `Match` (`id`, `idequipe1`, `idequipe2`) VALUES
(1,	1,	2),
(2,	4,	3);

DROP TABLE IF EXISTS `Pari`;
CREATE TABLE `Pari` (
  `id` int(11) NOT NULL,
  `idmatch` int(11) NOT NULL,
  `resultat` varchar(60) NOT NULL,
  `coteequipe1` double NOT NULL,
  `coteequipe2` double NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idmatch` (`idmatch`),
  CONSTRAINT `Pari_ibfk_1` FOREIGN KEY (`idmatch`) REFERENCES `Match` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `Utilisateur`;
CREATE TABLE `Utilisateur` (
  `id` int(11) NOT NULL,
  `nom` varchar(60) NOT NULL,
  `age` int(11) NOT NULL,
  `email` varchar(60) NOT NULL,
  `fond` double NOT NULL,
  `role` varchar(60) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `Utilisateur` (`id`, `nom`, `age`, `email`, `fond`, `role`) VALUES
(1,	'feef',	18,	'feef.gogo@gmail.com',	15.3,	'utilisateur'),
(2,	'faaf',	19,	'faaf.gogo@gmail.com',	12.25,	'utilisateur');

-- 2019-03-20 11:04:16
