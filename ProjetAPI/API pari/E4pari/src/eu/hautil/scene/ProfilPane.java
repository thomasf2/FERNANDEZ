package eu.hautil.scene;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import eu.hautil.DAO.*;
import eu.hautil.joueur.*;
import eu.hautil.utilisateur.*;
import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.stage.Stage;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;

public class ProfilPane extends GridPane {

	private Utilisateur utilisator;

	public void setUtilisateur(Utilisateur u) {

		this.utilisator = u;
		init();
	}
	public Utilisateur getUtilisateur() {
		return utilisator;
	}
	private void init() {
		UtilisateurDAO p = new UtilisateurDAO();
	
		TextField id = new TextField(String.valueOf(utilisator.getId()));
		id.setEditable(false);
		TextField mdp = new TextField(utilisator.getMdp());
		mdp.setEditable(false);
		TextField nom = new TextField(utilisator.getNom());
		TextField age = new TextField(String.valueOf(utilisator.getAge()));
		age.setEditable(false);
		TextField fond = new TextField(String.valueOf(utilisator.getFond()));
		fond.setEditable(false);
		TextField role = new TextField(utilisator.getRole());
		role.setEditable(false);
		TextField email = new TextField(utilisator.getEmail());
		email.setEditable(false);
		this.add(nom, 1, 2);
		this.add(age, 1, 3);
		this.add(fond, 1, 4);
		this.add(role, 1, 5);
		this.add(email, 1, 6);
		Button changerinfo = new Button();
		this.add(changerinfo, 2, 2);
		changerinfo.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent e) {
				int id2 = Integer.parseInt(id.getText());
				String nom2 = nom.getText();
				String mdp2 = mdp.getText();
				int age2 = Integer.parseInt(age.getText());
				int fond2 = Integer.parseInt(fond.getText());
				String role2 = role.getText();
				String email2 = email.getText();
				try {
					Utilisateur utilisateur2 = p.updateUtilisateur(id2, nom2);
					utilisator = utilisateur2;
				} catch (DAOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}

		}
				);
	}
	public ProfilPane() {

		this.setAlignment(Pos.CENTER);
		this.setHgap(10);
		this.setVgap(10);
		this.setPadding(new Insets(50,25,25,25));
		
		Text scenetitle = new Text("Connexion");

		scenetitle.setFont(Font.font("Tahoma", FontWeight.NORMAL,20));
		this.add(scenetitle,1,1,1,1);

		Button retour = new Button();
		this.add(retour,3,2);
		retour.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent e) {

				try {

					Main.getScene().setRoot(Main.getPanel("connectedPane"));
				} 
				catch (Exception ee) {

					ee.printStackTrace();
				}
			}
		});
	}}