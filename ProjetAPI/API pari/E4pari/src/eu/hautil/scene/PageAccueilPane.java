package eu.hautil.scene;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.layout.GridPane;

public class PageAccueilPane extends GridPane {
	

	public PageAccueilPane() {
		
		this.setAlignment(Pos.CENTER);
		this.setHgap(10);
		this.setVgap(10);
		
		Button inscription = new Button("Inscription");
		this.add(inscription, 1, 4);
		
		Button connexion = new Button("Connexion");
		this.add(connexion, 1, 5);
		inscription.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent e) {
				
				try {
				
					Main.getScene().setRoot(Main.getPanel("inscriptionPane"));
				} 
				catch (Exception ee) {

					ee.printStackTrace();
				}
			}
			

		});
		connexion.setOnAction(new EventHandler<ActionEvent>(){
			public void handle(ActionEvent e) {
				try {
					Main.getScene().setRoot(Main.getPanel("connexionPane"));
				}
				catch (Exception ee) {
					ee.printStackTrace();
				}
			}
		});
	}
}