package eu.hautil.scene;

import eu.hautil.DAO.UtilisateurDAO;
import eu.hautil.utilisateur.Utilisateur;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;

public class ConnexionPane extends GridPane { 

	private Utilisateur utilisator;
	private TextField usertextfield = new TextField();
	private PasswordField passfield = new PasswordField();
	private Text text = new Text();

	public void setUtilisateur(Utilisateur u) {

		this.utilisator = u;
		init();
	}	
	public Utilisateur getUtilisateur() {
		return utilisator;
	}
	private void init() {

	}


	public ConnexionPane() {

		this.setAlignment(Pos.CENTER);
		this.setHgap(10);
		this.setVgap(10);
		this.setId("connexionPane");
		Text scenetitle = new Text("Connexion");
		scenetitle.setFont(Font.font("Tahoma", FontWeight.NORMAL,20));
		this.add(scenetitle,1,1,1,1);
		UtilisateurDAO p = new UtilisateurDAO();
		this.add(usertextfield, 1, 3);
		this.add(passfield, 1, 4);
		Button connexion = new Button("Connexion");
		this.add(connexion, 1, 5);
		Button retour = new Button("Retour");
		this.add(retour, 2, 5);

		connexion.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent e) {

				try {
					String emailtest = usertextfield.getText();
					String mdptest = passfield.getText();
					Utilisateur h = p.connexionUtilisateur(emailtest, mdptest);
					if (h.getRole() == null){
						text.setText("Identifiants incorrects");
					}
					else if (h.getRole().equals("utilisateur")){
						UtilisateurDAO dao = new UtilisateurDAO();
						Utilisateur u = dao.connexionUtilisateur(emailtest, mdptest);
						UtilisateurPane uPane = (UtilisateurPane) Main.getPanel("utilisateurPane");
						uPane.setUtilisateur(u);
						Main.getScene().setRoot(uPane);
					}else if (h.getRole().equals("admin")) {

					}

				} 
				catch (Exception ee) {

					ee.printStackTrace();
				}
			}

		});
		retour.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent e) {
				Main.getScene().setRoot(Main.getPanel("accueilPane"));
			}
			
		});
	}
}