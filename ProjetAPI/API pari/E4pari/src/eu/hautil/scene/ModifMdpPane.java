package eu.hautil.scene;

import java.sql.SQLException;

import eu.hautil.DAO.DAOException;
import eu.hautil.DAO.UtilisateurDAO;
import eu.hautil.utilisateur.Utilisateur;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Text;

public class ModifMdpPane extends GridPane {
	private Utilisateur utilisator;

	public void setUtilisateur(Utilisateur u) {

		this.utilisator = u;
		init();

	}	
	public Utilisateur getUtilisateur() {
		return utilisator;
	}	
	private void init() {
		UtilisateurDAO p = new UtilisateurDAO();
		Text actualMdp = new Text();
		actualMdp.setText("Mot de passe actuel: " + utilisator.getMdp());
		Text info = new Text();
		info.setText("");
		Text saisie1 = new Text("Saisir le nouveau mot de passe");
		TextField newMdp1 = new TextField();
		Button valider = new Button("Ok");
		Button retour = new Button("Retour");
		this.add(saisie1, 1, 1);
		this.add(newMdp1, 1, 2);
		this.add(valider, 1, 3);
		this.add(info,1,4);
		this.add(retour, 7, 8);
		valider.setOnAction(new EventHandler<ActionEvent>(){

			@Override
			public void handle(ActionEvent e) {
				try {
					p.modifMdpUtilisateur(newMdp1.getText(), utilisator.getId()); 
					info.setText("Mot de passe actualis� avec succ�s.");
					
					if(newMdp1.getText().equals(actualMdp.getText())) {
						info.setText("Le mot de passe est le m�me.");
					}
					else {
						utilisator.setMdp(newMdp1.getText());
					}
				} catch (DAOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				};

			}

		});
		retour.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent arg0) {
				UtilisateurPane uPane = (UtilisateurPane) Main.getPanel("utilisateurPane");
				Main.getScene().setRoot(uPane);
			}

		});
		
	}
	public ModifMdpPane() {
		this.setAlignment(Pos.CENTER);
		this.setHgap(10);
		this.setVgap(10);

	}


}
