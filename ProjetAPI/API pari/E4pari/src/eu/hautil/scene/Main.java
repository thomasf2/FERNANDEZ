package eu.hautil.scene;

import java.util.Hashtable;

import eu.hautil.utilisateur.Utilisateur;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.ComboBox;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

public class Main extends Application {

	public ComboBox<Utilisateur> comboBox;
	ImageView iv;
	private static Scene scene;
	public static Hashtable<String, Pane> vues = new Hashtable<>();


	public static void initVues() {
		vues.put("inscriptionPane", new InscriptionPane());
		vues.put("bienvenuePane", new BienvenuePane());
		vues.put("accueilPane", new PageAccueilPane());
		vues.put("connexionPane", new ConnexionPane());
		vues.put("utilisateurPane", new UtilisateurPane());
		vues.put("modifmdpPane", new ModifMdpPane());
	}
	public static Pane getPanel(String s){
        return vues.get(s);
    }
	public static Utilisateur utilisateur;

																																																							 
    public static Scene getScene(){
        return scene;
    }

	public void start(Stage primaryStage) throws Exception{
				primaryStage.setMaximized(true);
		        initVues();
		        getPanel("accueilPane").setId("accueilPane");

		        scene = new Scene(getPanel("accueilPane"), 500, 500);
		        primaryStage.setTitle("Pari eSportif");
		        primaryStage.setScene(scene);
		        primaryStage.show();
				scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());

	}

	 public static void main(String[] args) {
	        launch(args);
	    }

	}
	
