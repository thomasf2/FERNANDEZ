package eu.hautil.scene;

import java.sql.SQLException;

import eu.hautil.DAO.DAOException;
import eu.hautil.DAO.EquipeDAO;
import eu.hautil.DAO.MatchDAO;
import eu.hautil.DAO.PariDAO;
import eu.hautil.DAO.PariUtilisateurDAO;
import eu.hautil.DAO.UtilisateurDAO;
import eu.hautil.equipe.Equipe;
import eu.hautil.match.Match;
import eu.hautil.pari.Pari;
import eu.hautil.pari.PariUtilisateur;
import eu.hautil.utilisateur.Utilisateur;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.TextField;
import javafx.scene.image.*;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;

public class UtilisateurPane extends GridPane {
	private Utilisateur utilisator;
	private Match match;
	private Equipe equipe;
	private Text id = new Text("");
	private int idpari;
	private Text test = new Text("");
	private int idutilisateur;
	private Text test2 = new Text("");
	private Text club = new Text("");
	private Text ratiowin = new Text("");
	private Text idUtilisateur = new Text("");
	private Text nomUtilisateur = new Text("");
	private Text mdpUtilisateur = new Text("");
	private Text ageUtilisateur = new Text("");
	private Text emailUtilisateur = new Text("");
	private Text fondUtilisateur = new Text("");
	private Text roleUtilisateur = new Text("");
	private Text idEquipe1 = new Text("");
	private Text idEquipe2 = new Text("");
	public Image logo = null;
	private int ida;
	private PariUtilisateur pariutilisateur;
	private Text coteEquipe1 = new Text("");
	private Text coteEquipe2 = new Text("");
	public ComboBox <Match> comboBox;
	public ComboBox <Equipe> comboBox1;
	public ComboBox <Pari> comboBox2;
	ObservableList<Equipe> liste;
	ObservableList<Match> listm;
	ObservableList<Pari> listp;
	ObservableList<Pari> listp1;
	ImageView iv = new ImageView();
	public void setUtilisateur(Utilisateur u) throws ClassNotFoundException, DAOException, SQLException {

		this.utilisator = u;
		init();

	}	
	public Utilisateur getUtilisateur() {
		return utilisator;
	}	
	private void init() throws DAOException, ClassNotFoundException, SQLException {
		idUtilisateur.setText("Id : "+utilisator.getId());
		idUtilisateur.setFill(Color.WHITE);
		nomUtilisateur.setText("Nom :"+utilisator.getNom());
		nomUtilisateur.setFill(Color.WHITE);
		mdpUtilisateur.setText("Mot de passe :"+utilisator.getMdp());
		mdpUtilisateur.setFill(Color.WHITE);
		ageUtilisateur.setText("Age :"+utilisator.getAge());
		ageUtilisateur.setFill(Color.WHITE);
		emailUtilisateur.setText("Email :"+utilisator.getEmail());
		emailUtilisateur.setFill(Color.WHITE);
		fondUtilisateur.setText("Fond :"+utilisator.getFond());
		fondUtilisateur.setFill(Color.WHITE);
		roleUtilisateur.setText("Role :"+utilisator.getRole());
		roleUtilisateur.setFill(Color.WHITE);
		PariUtilisateurDAO pariutilisateurdao = new PariUtilisateurDAO();
		UtilisateurDAO utilisateurdao = new UtilisateurDAO();
		MatchDAO m = new MatchDAO();
		GridPane grid = new GridPane();
		grid.setAlignment(Pos.CENTER);
		grid.setHgap(10);
		grid.setVgap(10);
		grid.setId("grideu");
		EquipeDAO e = new EquipeDAO();
		VBox vbox = new VBox();
		VBox vBoxLinks = new VBox();
		Button btnPari = new Button("Mes paris");
		this.add(btnPari, 1, 3);
		this.add(vbox, 1, 2);
		this.add(vBoxLinks, 2, 2);
		Hyperlink linkMdp = new Hyperlink();
		Hyperlink linkInfo = new Hyperlink();
		Hyperlink linkStats = new Hyperlink();
		Text listematch = new Text("Liste des matchs");
		listematch.setFill(Color.WHITE);
		this.add(listematch, 1, 5);
		Button listmatch = new Button("Voir + ");
		this.add(listmatch, 2, 6);
		e.getConnection();
		comboBox1 = new ComboBox<Equipe>();
		liste = e.rechercheAllEquipe();
		comboBox1.getItems().addAll(liste);
		comboBox1.setVisible(true);
		m.getConnection();
		comboBox = new ComboBox<Match>();
		listm = m.rechercheAllMatch();
		comboBox.getItems().addAll(listm); //recup�ration de la liste de la combobox et ajout de tous les joueurs -- comboBox.getItems() tout seul = renvoie juste la liste des elements mais pas d'ajout
		comboBox.setVisible(true);
		this.add(comboBox, 1, 6);
		this.add(idEquipe1, 1, 7);
		this.add(iv, 7, 9);
		this.add(club, 7, 7);
		this.add(coteEquipe1, 5, 6);
		this.add(coteEquipe2, 6, 6);
		this.add(ratiowin, 7,8);
		this.add(comboBox1, 6,7);
		Text versus = new Text("VS");
		this.add(versus, 8, 6);
		this.add(idEquipe2, 2, 7);
		Button voirplus = new Button("Voir plus");
		this.add(voirplus, 6, 8);
		vbox.getChildren().add(idUtilisateur);
		vbox.getChildren().add(nomUtilisateur);
		vbox.getChildren().add(mdpUtilisateur);
		vbox.getChildren().add(ageUtilisateur);
		vbox.getChildren().add(emailUtilisateur);
		vbox.getChildren().add(fondUtilisateur);
		vbox.getChildren().add(roleUtilisateur);
		PariDAO pariDAO = new PariDAO();
		idutilisateur = utilisator.getId();
		pariDAO.getConnection();
		comboBox2 = new ComboBox<Pari>();
		listp = pariDAO.recherchePari(idutilisateur);
		comboBox2.getItems().addAll(listp);
		comboBox2.setVisible(false);
		this.add(comboBox2, 1, 4);
		Text pariez = new Text("Ajoutez un pari");
		Text idparitxt = new Text("Id pari :");
		Text fondutilisetxt = new Text("Fond utilis�");
		TextField fondutilise = new TextField();
		TextField idpari = new TextField();
		Button pariezbtn = new Button("OK");
		grid.add(pariez, 10, 1);
		grid.add(fondutilisetxt, 8, 2);
		grid.add(fondutilise, 9, 2);
		grid.add(idparitxt,10, 2);
		grid.add(idpari, 11, 2);
		grid.add(pariezbtn, 12, 2);
		this.add(grid, 10, 1);
		vBoxLinks.getChildren().addAll(linkInfo,linkMdp,linkStats);
		linkMdp.setText("Modifier le mot de passe");
		linkMdp.setTextFill(Color.RED);
		linkMdp.setOnAction(new EventHandler<ActionEvent>(){

			@Override
			public void handle(ActionEvent e) {
				ModifMdpPane mPane = (ModifMdpPane) Main.getPanel("modifmdpPane");
				mPane.setUtilisateur(utilisator);
				Main.getScene().setRoot(mPane);

			}

		});
		listmatch.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent arg0) {

				try {
					Match match1 = new Match();
					Equipe ekip1 = new Equipe();
					Equipe ekip2 = new Equipe();
					match1 = comboBox.getValue();
					ekip1 = e.rechercheEquipe(match1.getIdEquipe1());
					ekip2 = e.rechercheEquipe(match1.getIdEquipe2());
					idEquipe1.setText("Equipe 1 :" +ekip1.getClub());
					idEquipe1.setFill(Color.RED);
					idEquipe2.setText("Equipe 2 :" +ekip2.getClub());
					idEquipe2.setFill(Color.YELLOW);
				} catch (DAOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}



			}

		});
		voirplus.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent ee) {
				try {
					Equipe ekip1 = new Equipe();
					ekip1 = comboBox1.getValue();
					id.setText("Id : "+ekip1.getIdEquipe());
					id.setFill(Color.WHITE);
					club.setText("Club : "+ekip1.getClub());
					club.setFill(Color.WHITE);
					ratiowin.setText("Ratio Victoire/D�faite : "+ekip1.getRatiowin());
					ratiowin.setFill(Color.WHITE);
					logo = new Image(ekip1.getLogo());
					iv.setImage(logo);
					iv.setFitHeight(150);
					iv.setFitWidth(150);
				}
				catch(Exception e){
					e.printStackTrace();
					
				}


			}

		});
		btnPari.setOnAction(new EventHandler<ActionEvent>(){

			@Override
			public void handle(ActionEvent e) {
				comboBox2.setVisible(true);
			}
		});
		pariezbtn.setOnAction(new EventHandler<ActionEvent>(){

			@Override
			public void handle(ActionEvent e) {
				int fondutilise1 = Integer.parseInt(fondutilise.getText());
				int idpari1 = Integer.parseInt(idpari.getText());
				int fondUtilisateur1 =utilisator.getFond();
				String email1 = utilisator.getEmail();
				
				try {
					pariutilisateurdao.ajoutPariUtilisateur(idutilisateur, fondutilise1, idpari1);
					int newfond = utilisateurdao.retirerFondUtilisateur(fondUtilisateur1, fondutilise1, email1);
					utilisator.setFond(newfond);
					fondUtilisateur.setText("Fond :"+utilisator.getFond());
					listp1 = pariDAO.recherchePari(idutilisateur);
					comboBox2.getItems().addAll(listp1);
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
	}		
	public UtilisateurPane() {
		this.setAlignment(Pos.CENTER);
		this.setHgap(50);
		this.setVgap(50);
		this.setId("utilisateurPane");
		Button profil = new Button("Profil");

	}


}
