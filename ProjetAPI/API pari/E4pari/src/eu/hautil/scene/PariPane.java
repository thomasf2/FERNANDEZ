package eu.hautil.scene;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import eu.hautil.DAO.*;
import eu.hautil.joueur.*;
import eu.hautil.match.Match;
import eu.hautil.utilisateur.*;
import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.stage.Stage;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;

public class PariPane extends GridPane {
	private Utilisateur utilisator;
	private Match match;
	private Text idUtilisateur = new Text("");
	private Text nomUtilisateur = new Text("");
	private Text mdpUtilisateur = new Text("");
	private Text ageUtilisateur = new Text("");
	private Text emailUtilisateur = new Text("");
	private Text fondUtilisateur = new Text("");
	private Text roleUtilisateur = new Text("");
	private Text idEquipe1 = new Text("");
	private Text idEquipe2 = new Text("");
	private Text idMatch = new Text("");
	public void setUtilisateur(Utilisateur u) {

		this.utilisator = u;
		init();

	}	
	public Utilisateur getUtilisateur() {
		return utilisator;
	}	
	private void init() {
		idUtilisateur.setText("Id : "+utilisator.getId());
		nomUtilisateur.setText("Nom :"+utilisator.getNom());
		mdpUtilisateur.setText("Mot de passe :"+utilisator.getMdp());
		ageUtilisateur.setText("Age :"+utilisator.getAge());
		emailUtilisateur.setText("Email :"+utilisator.getEmail());
		fondUtilisateur.setText("Fond :"+utilisator.getFond());
		roleUtilisateur.setText("Role :"+utilisator.getRole());

		UtilisateurDAO p = new UtilisateurDAO();
		VBox vbox = new VBox();
		VBox vBoxLinks = new VBox();
		Button btnDeco = new Button("Deconnexion");
		this.add(vbox, 1, 2);
		this.add(vBoxLinks, 2, 2);
		this.add(btnDeco, 7, 8);
	    Hyperlink linkMdp = new Hyperlink();
	    Hyperlink linkInfo = new Hyperlink();
	    Hyperlink linkStats = new Hyperlink();
	    Button listmatch = new Button("Liste des matchs");
	    this.add(listmatch, 1, 9);
		vbox.getChildren().add(idUtilisateur);
		vbox.getChildren().add(nomUtilisateur);
		vbox.getChildren().add(mdpUtilisateur);
		vbox.getChildren().add(ageUtilisateur);
		vbox.getChildren().add(emailUtilisateur);
		vbox.getChildren().add(fondUtilisateur);
		vbox.getChildren().add(roleUtilisateur);
		vBoxLinks.getChildren().addAll(linkInfo,linkMdp,linkStats);
		linkMdp.setText("Modifier le mot de passe");
		linkMdp.setOnAction(new EventHandler<ActionEvent>(){

			@Override
			public void handle(ActionEvent e) {
				ModifMdpPane mPane = (ModifMdpPane) Main.getPanel("modifmdpPane");
				mPane.setUtilisateur(utilisator);
				Main.getScene().setRoot(mPane);
				
			}
		
		});
		listmatch.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent arg0) {
				
				
			}
			
		});

	}
	public PariPane() {
		this.setAlignment(Pos.CENTER);
		this.setHgap(10);
		this.setVgap(10);
		Text scenetitle = new Text("Connexion");
		scenetitle.setFont(Font.font("Tahoma", FontWeight.NORMAL,20));
		this.add(scenetitle,1,1,1,1);
		
		Button profil = new Button("Profil");
		
}


}
