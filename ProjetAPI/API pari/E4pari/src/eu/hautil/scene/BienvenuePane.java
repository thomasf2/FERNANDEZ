package eu.hautil.scene;

import eu.hautil.DAO.UtilisateurDAO;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;

public class BienvenuePane extends GridPane  {
	

	public BienvenuePane() {
	
		this.setAlignment(Pos.CENTER);
		this.setHgap(10);
		this.setVgap(10);
		this.setBackground(new Background(new BackgroundFill(Color.DARKGOLDENROD,CornerRadii.EMPTY,Insets.EMPTY)));
		this.setPadding(new Insets(50,25,25,25));
		UtilisateurDAO b = new UtilisateurDAO();
	

		Text scenetitle = new Text("Bienvenue!");
		scenetitle.setFont(Font.font("Tahoma", FontWeight.NORMAL,20));
		Text t = new Text("Bienvenue sur le site de Pari eSportf");
		this.add(t, 1, 1);
		Text e = new Text("Cadeau de bienvenue : 20FONDS");
		this.add(e, 1, 2);
		Button bienvenue = new Button("Visitez le site !");
		this.add(bienvenue, 1, 4);
		
		bienvenue.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent e) {
				
				try {
				
					Main.vues.put("bienvenuePane",new BienvenuePane());
					Main.getScene().setRoot(Main.getPanel("connexionPane"));
				} 
				catch (Exception ee) {

					ee.printStackTrace();
				}
			}
			

		});
		/*
		comboBox= new ComboBox <Utilisateur>();
		list = daoutilisateur.rechercheAllUtilisateur();
		comboBox.getItems().addAll(list); //recupèration de la liste de la combobox et ajout de tous les joueurs -- comboBox.getItems() tout seul = renvoie juste la liste des elements mais pas d'ajout
		TextField identifiant = new TextField();
		grid.add(identifiant, 1, 2);
		TextField mdp = new TextField();
		grid.add(mdp, 1, 3);
		Text id = new Text("Identifiant");
		grid.add(id, 0, 2);
		Text motdp = new Text("Mot de passe");
		grid.add(motdp, 0, 3);

		Button btn = new Button("OK");
		HBox hbBtn = new HBox(10);
		hbBtn.setAlignment(Pos.BOTTOM_RIGHT);
		hbBtn.getChildren().add(btn);
		grid.add(hbBtn, 1, 4);
		/*Text id = new Text();
		grid.add(id,0, 4);
		Text idequipe = new Text();
		grid.add(idequipe,0, 5);
		Text poste = new Text();
		grid.add(poste,0, 6);
		Text pseudo = new Text();
		grid.add(pseudo,0, 7);
		Text ratio = new Text();
		grid.add(ratio,0, 8);
		Text nom = new Text();
		grid.add(nom,0, 9);
		TextField identifiant = new TextField("id");
		grid.add(identifiant, 0, 10);
		TextField password = new TextField("mdp");
		grid.add(password, 0, 11);



		btn.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent e) {

				Joueurs j;
				String url ="hand-311121_640.png";
				Image image = new Image(url, 100, 100, false, true);
				ImageView iv = new ImageView(image);
				grid.add(iv, 3, 3);
				j = comboBox.getValue(); // récupèration objet joueur selectionné
				System.out.println(j);
				id.setText("Id :"+j.getId());
				idequipe.setText("Equipe : "+j.getIdequipe());
				poste.setText("Poste : "+j.getPoste());
				pseudo.setText("Club : "+j.getPseudo());
				ratio.setText("Prenom : "+j.getRatio());
				nom.setText("Numéro : "+j.getNom());}



		}); 

		 */

		//textfield1.setText(comboBox.getSelectionModel().getSelectedItem().getIdentifiant());




	}

}