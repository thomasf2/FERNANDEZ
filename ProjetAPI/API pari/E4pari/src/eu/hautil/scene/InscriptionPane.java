package eu.hautil.scene;

import eu.hautil.DAO.UtilisateurDAO;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;

public class InscriptionPane extends GridPane {
	

	public InscriptionPane() {
		
		this.setAlignment(Pos.CENTER);
		this.setHgap(10);
		this.setVgap(10);
		this.setId("inscriptionPane");
		UtilisateurDAO p = new UtilisateurDAO();
		Text scenetitle = new Text("Inscription");
		scenetitle.setFont(Font.font("Tahoma", FontWeight.NORMAL,20));
		this.add(scenetitle,1,1,1,1);
		TextField mdp = new TextField();
		this.add(mdp, 1, 2);
		Label mdpt = new Label("mdp :");
		this.add(mdpt, 0, 2);
		TextField nom = new TextField();
		this.add(nom, 1, 3);
		Label nomt = new Label("nom :");
		this.add(nomt, 0, 3);
		TextField age = new TextField();
		this.add(age, 1, 4);
		Label aget = new Label("age :");
		this.add(aget, 0, 4);
		TextField email = new TextField();
		this.add(email, 1, 5);
		Label emailt = new Label("email :");
		this.add(emailt, 0, 5);
		
		Button inscription = new Button("Inscription");
		this.add(inscription, 1,6 );
		Button retour = new Button("Retour");
		this.add(retour, 2, 6);
		inscription.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent e) {
				
				try {
					String nomget = nom.getText();
					String mdpget = mdp.getText();
					int agetest = Integer.parseInt(age.getText());
					String emailget = email.getText();
					p.ajoutUtilisateur(nomget,mdpget, agetest, emailget, 20, "utilisateur");
					if (p == null) {
						System.out.println("error");
					}
					else {
						Main.getScene().setRoot(Main.getPanel("connexionPane"));
					}
				} 
				catch (Exception ee) {

					ee.printStackTrace();
				}
			}
			

		});
		retour.setOnAction(new EventHandler<ActionEvent>() {
			public void handle (ActionEvent e) {
					Main.getScene().setRoot(Main.getPanel("accueilPane"));
		
					
				
			}
		});
			
		/*
		comboBox= new ComboBox <Utilisateur>();
		list = daoutilisateur.rechercheAllUtilisateur();
		comboBox.getItems().addAll(list); //recupèration de la liste de la combobox et ajout de tous les joueurs -- comboBox.getItems() tout seul = renvoie juste la liste des elements mais pas d'ajout
		TextField identifiant = new TextField();
		grid.add(identifiant, 1, 2);
		TextField mdp = new TextField();
		grid.add(mdp, 1, 3);
		Text id = new Text("Identifiant");
		grid.add(id, 0, 2);
		Text motdp = new Text("Mot de passe");
		grid.add(motdp, 0, 3);

		Button btn = new Button("OK");
		HBox hbBtn = new HBox(10);
		hbBtn.setAlignment(Pos.BOTTOM_RIGHT);
		hbBtn.getChildren().add(btn);
		grid.add(hbBtn, 1, 4);
		/*Text id = new Text();
		grid.add(id,0, 4);
		Text idequipe = new Text();
		grid.add(idequipe,0, 5);
		Text poste = new Text();
		grid.add(poste,0, 6);
		Text pseudo = new Text();
		grid.add(pseudo,0, 7);
		Text ratio = new Text();
		grid.add(ratio,0, 8);
		Text nom = new Text();
		grid.add(nom,0, 9);
		TextField identifiant = new TextField("id");
		grid.add(identifiant, 0, 10);
		TextField password = new TextField("mdp");
		grid.add(password, 0, 11);



		btn.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent e) {

				Joueurs j;
				String url ="hand-311121_640.png";
				Image image = new Image(url, 100, 100, false, true);
				ImageView iv = new ImageView(image);
				grid.add(iv, 3, 3);
				j = comboBox.getValue(); // récupèration objet joueur selectionné
				System.out.println(j);
				id.setText("Id :"+j.getId());
				idequipe.setText("Equipe : "+j.getIdequipe());
				poste.setText("Poste : "+j.getPoste());
				pseudo.setText("Club : "+j.getPseudo());
				ratio.setText("Prenom : "+j.getRatio());
				nom.setText("Numéro : "+j.getNom());}



		}); 

		 */

		//textfield1.setText(comboBox.getSelectionModel().getSelectedItem().getIdentifiant());




	}

}