package eu.hautil.DAO;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import eu.hautil.equipe.Equipe;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
/**
 * Classe permettant la connexion a la base de donn�e avec la classe utilisateur
 * @author Thomas
 *
 */
public class EquipeDAO {
	/**
	 * Methode qui permet de se connecter � la base de donn�e
	 * @return
	 * @throws SQLException
	 * @throws ClassNotFoundException
	 */
	public Connection getConnection() throws SQLException,ClassNotFoundException {
		Connection conn = null;
		String driver = "com.mysql.cj.jdbc.Driver";
		String url = "jdbc:mysql://totodb.c2xrkvuey9hl.eu-west-1.rds.amazonaws.com:3306/ProjetAPIE4?useSSL=false";
		String user="fernat"; 
		String mdp = "thomasf2";
		try{
			Class.forName(driver);
			System.out.println("driver ok");
			conn=DriverManager.getConnection(url,user,mdp);
			System.out.println("connection ok");

		}catch(Exception e){
			e.printStackTrace();
		}
		return conn;
	}
	public Equipe rechercheEquipe (int id) throws DAOException {
		Equipe equipe = null;
		PreparedStatement pstmt3 = null;
		try {
			pstmt3 = getConnection().prepareStatement("SELECT * FROM Equipe WHERE id = ?");
			pstmt3.setInt(1,id);
			ResultSet equipeId = pstmt3.executeQuery();
			if(equipeId.next()) 
				equipe = new Equipe(equipeId.getInt(1),equipeId.getInt(2),equipeId.getInt(3), equipeId.getString(4),equipeId.getString(5),equipeId.getString(6));

			equipeId.close();
			pstmt3.close();
		}catch (SQLException e) {
			DAOException dao = new DAOException ("Erreur SQL",e);
			throw dao;
		}
		catch (ClassNotFoundException e) {
			DAOException dao = new DAOException ("Erreur Driver",e);
			throw dao;
		}
		finally {
			try {
				if (pstmt3 != null) {
					pstmt3.close();
				}
			}
			catch (SQLException e) {
				e.printStackTrace();
			}
		}


		return equipe;
	}
	public ObservableList<Equipe> rechercheAllEquipe() throws DAOException {
		ArrayList<Equipe> e = new ArrayList<Equipe>();
		ResultSet resId2=null;
		PreparedStatement pstmt3;
		try {
			pstmt3 = getConnection().prepareStatement("SELECT * FROM Equipe");
			resId2 = pstmt3.executeQuery();
			while(resId2.next()) {
				Equipe res = new Equipe(resId2.getInt(1),resId2.getInt(2),resId2.getInt(3),resId2.getString(4),resId2.getString(5),resId2.getString(6));
				e.add(res);
			}
			pstmt3.close();
			ObservableList<Equipe> list = FXCollections.observableArrayList();
			list.addAll(e);
			return list;

		} catch (SQLException e1) {
			DAOException dao = new DAOException ("Erreur SQL",e1);
			throw dao;
		}
		catch (ClassNotFoundException e1) {
			DAOException dao = new DAOException ("Erreur Driver",e1);
			throw dao;
		}
		finally {
			try {
				if (resId2 != null) {
					resId2.close();
				}
			}
			catch (SQLException e1) {
				e1.printStackTrace();
			}
		}

	}


}
