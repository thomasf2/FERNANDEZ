package eu.hautil.DAO;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import eu.hautil.match.Match;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
/**
 * Classe permettant la connexion a la base de donn�e avec la classe utilisateur
 * @author Thomas
 *
 */
public class MatchDAO {
	/**
	 * Methode qui permet de se connecter � la base de donn�e
	 * @return
	 * @throws SQLException
	 * @throws ClassNotFoundException
	 */
	public Connection getConnection() throws SQLException,ClassNotFoundException {
		Connection conn = null;
		String driver = "com.mysql.cj.jdbc.Driver";
		String url = "jdbc:mysql://totodb.c2xrkvuey9hl.eu-west-1.rds.amazonaws.com:3306/ProjetAPIE4?useSSL=false";
		String user="fernat"; 
		String mdp = "thomasf2";
		try{
			Class.forName(driver);
			System.out.println("driver ok");
			conn=DriverManager.getConnection(url,user,mdp);
			System.out.println("connection ok");

		}catch(Exception e){
			e.printStackTrace();
		}
		return conn;
	}
	public void ajoutMatch(int id , int idEquipe1 , int idEquipe2) throws Exception {
		PreparedStatement pstmt = getConnection().prepareStatement("INSERT INTO Match VALUES (?,?,?)");
		pstmt.setInt(1,id);
		pstmt.setInt(2,idEquipe1);
		pstmt.setInt(3,idEquipe2);
		int res=pstmt.executeUpdate();
		System.out.println(res);
	}
	public Match rechercheMatch (int id) throws DAOException {
		Match match = null;
		PreparedStatement pstmt3 = null;
		try {
			pstmt3 = getConnection().prepareStatement("SELECT * FROM Match WHERE id = ?");
			pstmt3.setInt(1,id);
			ResultSet matchId = pstmt3.executeQuery();
			if(matchId.next()) 
				match = new Match(matchId.getInt(1),matchId.getInt(2),matchId.getInt(3));

			matchId.close();
			pstmt3.close();
		}catch (SQLException e) {
			DAOException dao = new DAOException ("Erreur SQL",e);
			throw dao;
		}
		catch (ClassNotFoundException e) {
			DAOException dao = new DAOException ("Erreur Driver",e);
			throw dao;
		}
		finally {
			try {
				if (pstmt3 != null) {
					pstmt3.close();
				}
			}
			catch (SQLException e) {
				e.printStackTrace();
			}
		}


		return match;
	}
	public ObservableList<Match> rechercheAllMatch() throws DAOException {
		ArrayList<Match> m = new ArrayList<Match>();
		ResultSet resId2=null;
		PreparedStatement pstmt3;
		try {
			pstmt3 = getConnection().prepareStatement("SELECT * FROM `Match`");
			resId2 = pstmt3.executeQuery();
			while(resId2.next()) {
				Match res = new Match(resId2.getInt(1),resId2.getInt(2),resId2.getInt(3));
				m.add(res);
			}
			pstmt3.close();
			ObservableList<Match> list = FXCollections.observableArrayList();
			list.addAll(m);
			return list;

		} catch (SQLException e) {
			DAOException dao = new DAOException ("Erreur SQL",e);
			throw dao;
		}
		catch (ClassNotFoundException e) {
			DAOException dao = new DAOException ("Erreur Driver",e);
			throw dao;
		}
		finally {
			try {
				if (resId2 != null) {
					resId2.close();
				}
			}
			catch (SQLException e) {
				e.printStackTrace();
			}
		}

	}


}
