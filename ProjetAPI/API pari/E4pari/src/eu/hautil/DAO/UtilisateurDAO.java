package eu.hautil.DAO;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import eu.hautil.utilisateur.Utilisateur;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
/**
 * Classe permettant la connexion a la base de donn�e avec la classe utilisateur
 * @author Thomas
 *
 */
public class UtilisateurDAO {
	/**
	 * Methode qui permet de se connecter � la base de donn�e
	 * @return
	 * @throws SQLException
	 * @throws ClassNotFoundException
	 */
	public Connection getConnection() throws SQLException,ClassNotFoundException {
		Connection conn = null;
		String driver = "com.mysql.cj.jdbc.Driver";
		String url = "jdbc:mysql://totodb.c2xrkvuey9hl.eu-west-1.rds.amazonaws.com:3306/ProjetAPIE4?useSSL=false";
		String user="fernat"; 
		String mdp = "thomasf2";
		try{
			Class.forName(driver);
			System.out.println("driver ok");
			conn=DriverManager.getConnection(url,user,mdp);
			System.out.println("connection ok");

		}catch(Exception e){
			e.printStackTrace();
		}
		return conn;
	}
	/**
	 * La m�thode ajoute un utilisateur avec des param�tres entrer.
	 * @param id
	 * @param nom
	 * @param age
	 * @param email
	 * @param fond
	 * @param role
	 * @throws Exception
	 */
	public void modiffondUtilisateur(int fond, String email) throws DAOException {
		PreparedStatement pstmt10=null;
		try {
			pstmt10 = getConnection().prepareStatement("UPDATE Utilisateur SET fond = ? WHERE email = ?");
			pstmt10.setDouble(1, fond);
			pstmt10.setString(2, email);
			pstmt10.executeUpdate();
		}
		catch(SQLException e) {
			DAOException dao = new DAOException ("Erreur SQL",e);
			throw dao;
		}
		catch(ClassNotFoundException e) {
			DAOException dao = new DAOException ("Erreur Driver",e);
			throw dao;
		}
	}
	public int retirerFondUtilisateur(int fond , int fondretirer,String email) throws DAOException {
		PreparedStatement pstmt10=null;
		int newfond = fond - fondretirer;
		try {
			pstmt10 = getConnection().prepareStatement("UPDATE Utilisateur SET fond = ? WHERE email = ?");
			pstmt10.setInt(1, newfond);
			pstmt10.setString(2, email);
			pstmt10.executeUpdate();
			pstmt10.close();
		}
		catch(SQLException e) {
			DAOException dao = new DAOException ("Erreur SQL",e);
			throw dao;
		}
		catch(ClassNotFoundException e) {
			DAOException dao = new DAOException ("Erreur Driver",e);
			throw dao;
		}
		return newfond;
	}
	public void modifMdpUtilisateur(String mdp ,int id) throws DAOException {
		PreparedStatement pstmt10=null;
		try {
			pstmt10 = getConnection().prepareStatement("UPDATE Utilisateur SET mdp = ? WHERE id = ?");
			pstmt10.setString(1, mdp);
			pstmt10.setInt(2, id);
			pstmt10.executeUpdate();		}
		catch(SQLException e) {
			DAOException dao = new DAOException ("Erreur SQL",e);
			throw dao;
		}
		catch(ClassNotFoundException e) {
			DAOException dao = new DAOException ("Erreur Driver",e);
			throw dao;
		}
	}
	/**
	 * La m�thode supprime un utilisateur en fonction de son id
	 * @param id
	 * @return 
	 * @throws Exception
	 */
	public void supprimerUtilisateur(int id) throws DAOException {
		PreparedStatement pstmt = null;
		try {
			pstmt = getConnection().prepareStatement("DELETE FROM Utilisateur WHERE id = ?");
			pstmt.setInt(1, id);
			int res = pstmt.executeUpdate();
			System.out.println(res);
		}
		catch(SQLException e) {
			DAOException dao = new DAOException ("Erreur SQL",e);
			throw dao;
		}
		catch(ClassNotFoundException e) {
			DAOException dao = new DAOException ("Erreur Driver",e);
			throw dao;
		}
	}
	public void ajoutUtilisateur(String nom,String mdp, int age, String email, int fond, String role) throws Exception {
		PreparedStatement pstmt = getConnection().prepareStatement("INSERT INTO Utilisateur (nom,mdp,age,email,fond,role) VALUES (?,?,?,?,?,?)");
		pstmt.setString(1,nom);
		pstmt.setString(2,mdp);
		pstmt.setInt(3,age);
		pstmt.setString(4,email);
		pstmt.setInt(5,fond);
		pstmt.setString(6,role);
		int res=pstmt.executeUpdate();
		System.out.println(res);
	}
	/**
	 * La m�thode recherche un utilisateur en fonction de son id
	 * @param id
	 * @return 
	 * @throws Exception
	 */
	public Utilisateur rechercheUtilisateur(int id) throws DAOException {

		Utilisateur res = null;
		PreparedStatement pstmt3=null;
		try {
			pstmt3 = getConnection().prepareStatement("SELECT * FROM Utilisateur WHERE identifiant = ?");
			pstmt3.setInt(1,id);
			ResultSet resId = pstmt3.executeQuery();
			if(resId.next()) 
				res = new Utilisateur(resId.getInt(1),resId.getString(2),resId.getString(3),resId.getInt(4),resId.getString(5),resId.getInt(6),resId.getString(7));

			resId.close();
			pstmt3.close();
		}catch (SQLException e) {
			DAOException dao = new DAOException ("Erreur SQL",e);
			throw dao;
		}
		catch (ClassNotFoundException e) {
			DAOException dao = new DAOException ("Erreur Driver",e);
			throw dao;
		}
		finally {
			try {
				if (pstmt3 != null) {
					pstmt3.close();
				}
			}
			catch (SQLException e) {
				e.printStackTrace();
			}
		}


		return res;
	}
	public Utilisateur connexionUtilisateur(String email, String mdp) throws DAOException {
		PreparedStatement pstmt6 =null;
		Utilisateur user=null;
		try {
			pstmt6 = getConnection().prepareStatement("SELECT * FROM Utilisateur WHERE email = ? AND mdp = ?");
			pstmt6.setString(1, email);
			pstmt6.setString(2, mdp);
			ResultSet result = pstmt6.executeQuery();
			while(result.next()) {
				user = new Utilisateur();
				user.setId(result.getInt(1));
				user.setNom(result.getString(2));
				user.setAge(result.getInt(4));
				user.setEmail(result.getString(5));
				user.setFond(result.getInt(6));
				user.setRole(result.getString(7));

			}
		}
		catch (SQLException e) {
			DAOException dao = new DAOException ("Erreur SQL",e);
			throw dao;
		}
		catch (ClassNotFoundException e) {
			DAOException dao = new DAOException ("Erreur Driver",e);
			throw dao;
		}
		finally {
			try {
				if (pstmt6 != null) {
					pstmt6.close();
				}
			}
			catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return user;
	}
	public Utilisateur updateUtilisateur(int id, String nom) throws DAOException {
		PreparedStatement pstmt9 = null;
		Utilisateur user = null;
		try {
			pstmt9 = getConnection().prepareStatement("UPDATE Utilisateur SET nom = ? WHERE id = ?");
			pstmt9.setString(1, nom);
			pstmt9.setInt(2, id);
			
			
				
			}
		catch (SQLException e) {
			DAOException dao = new DAOException ("Erreur SQL",e);
			throw dao;
		}
		catch (ClassNotFoundException e) {
			DAOException dao = new DAOException ("Erreur Driver",e);
			throw dao;
		}
		return user;
	}
	/**
	 * Methode qui permet de regarder la liste des utilisateurs
	 * @return
	 * @throws DAOException
	 */
	
	public ObservableList<Utilisateur> rechercheAllUtilisateur() throws DAOException {
		ArrayList<Utilisateur> u = new ArrayList<Utilisateur>();
		ResultSet resId2=null;
		PreparedStatement pstmt3;
		try {
			pstmt3 = getConnection().prepareStatement("SELECT * FROM Utilisateur");
			resId2 = pstmt3.executeQuery();
			while(resId2.next()) {
				Utilisateur res = new Utilisateur(resId2.getInt(1),resId2.getString(2),resId2.getString(3),resId2.getInt(4),resId2.getString(5),resId2.getInt(6),resId2.getString(7));
				u.add(res);
			}
			pstmt3.close();
			ObservableList<Utilisateur> list = FXCollections.observableArrayList();
			list.addAll(u);
			return list;

		} catch (SQLException e) {
			DAOException dao = new DAOException ("Erreur SQL",e);
			throw dao;
		}
		catch (ClassNotFoundException e) {
			DAOException dao = new DAOException ("Erreur Driver",e);
			throw dao;
		}
		finally {
			try {
				if (resId2 != null) {
					resId2.close();
				}
			}
			catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
}
