package eu.hautil.DAO;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import eu.hautil.pari.Pari;
import eu.hautil.pari.PariUtilisateur;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class PariUtilisateurDAO {
	public Connection getConnection() throws SQLException,ClassNotFoundException {
		Connection conn = null;
		String driver = "com.mysql.cj.jdbc.Driver";
		String url = "jdbc:mysql://totodb.c2xrkvuey9hl.eu-west-1.rds.amazonaws.com:3306/ProjetAPIE4?useSSL=false";
		String user="fernat"; 
		String mdp = "thomasf2";
		try{
			Class.forName(driver);
			System.out.println("driver ok");
			conn=DriverManager.getConnection(url,user,mdp);
			System.out.println("connection ok");

		}catch(Exception e){
			e.printStackTrace();
		}
		return conn;
	}
	public void ajoutPariUtilisateur(int idutilisateur, int fondutilise, int idpari) throws Exception {
		PreparedStatement pstmt = getConnection().prepareStatement("INSERT INTO PariUtilisateur(idutilisateur,fondutilise,idpari) VALUES (?,?,?)");
		pstmt.setInt(1,idutilisateur);
		pstmt.setInt(2,fondutilise);
		pstmt.setInt(3, idpari);
		int res=pstmt.executeUpdate();
		System.out.println(res);
	}
	public ObservableList<PariUtilisateur> recherchePariUtilisateur (int id) throws DAOException {
		ArrayList<PariUtilisateur> pariUtilisateur = new ArrayList<PariUtilisateur>();
		ResultSet pariId=null;
		PreparedStatement pstmt3;
		try {
			pstmt3 = getConnection().prepareStatement("SELECT * FROM PariUtilisateur WHERE id = ?");
			pstmt3.setInt(1,id);
			pariId = pstmt3.executeQuery();
			while(pariId.next()) { 
				PariUtilisateur pari1 = new PariUtilisateur(pariId.getInt(1),pariId.getInt(2),pariId.getInt(3), pariId.getInt(4));
				pariUtilisateur.add(pari1);
		}
			pariId.close();
			pstmt3.close();
			ObservableList<PariUtilisateur> list = FXCollections.observableArrayList();
			list.addAll(pariUtilisateur);
			return list;
			}
		
			catch (SQLException e) {
			DAOException dao = new DAOException ("Erreur SQL",e);
			throw dao;
		}
		catch (ClassNotFoundException e) {
			DAOException dao = new DAOException ("Erreur Driver",e);
			throw dao;
		}
		finally {
			try {
				if (pariId != null) {
					pariId.close();
				}
			}
			catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
	public ObservableList<PariUtilisateur> rechercheAllPariUtilisateur() throws DAOException {
		ArrayList<PariUtilisateur> p = new ArrayList<PariUtilisateur>();
		ResultSet resId2=null;
		PreparedStatement pstmt3;
		try {
			pstmt3 = getConnection().prepareStatement("SELECT * FROM PariUtilisateur");
			resId2 = pstmt3.executeQuery();
			while(resId2.next()) {
				PariUtilisateur res = new PariUtilisateur(resId2.getInt(1),resId2.getInt(2),resId2.getInt(3),resId2.getInt(4));
				p.add(res);
			}
			pstmt3.close();
			ObservableList<PariUtilisateur> list = FXCollections.observableArrayList();
			list.addAll(p);
			return list;

		} catch (SQLException e1) {
			DAOException dao = new DAOException ("Erreur SQL",e1);
			throw dao;
		}
		catch (ClassNotFoundException e1) {
			DAOException dao = new DAOException ("Erreur Driver",e1);
			throw dao;
		}
		finally {
			try {
				if (resId2 != null) {
					resId2.close();
				}
			}
			catch (SQLException e1) {
				e1.printStackTrace();
			}
		}

	}

}
