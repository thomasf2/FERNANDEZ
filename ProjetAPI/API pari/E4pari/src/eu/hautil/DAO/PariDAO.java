package eu.hautil.DAO;

	import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import eu.hautil.pari.Pari;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
public class PariDAO {

	/**
	 * Classe permettant la connexion a la base de donn�e avec la classe utilisateur
	 * @author Thomas
	 *
	 */
		/**
		 * Methode qui permet de se connecter � la base de donn�e
		 * @return
		 * @throws SQLException
		 * @throws ClassNotFoundException
		 */
		public Connection getConnection() throws SQLException,ClassNotFoundException {
			Connection conn = null;
			String driver = "com.mysql.cj.jdbc.Driver";
			String url = "jdbc:mysql://totodb.c2xrkvuey9hl.eu-west-1.rds.amazonaws.com:3306/ProjetAPIE4?useSSL=false";
			String user="fernat"; 
			String mdp = "thomasf2";
			try{
				Class.forName(driver);
				System.out.println("driver ok");
				conn=DriverManager.getConnection(url,user,mdp);
				System.out.println("connection ok");

			}catch(Exception e){
				e.printStackTrace();
			}
			return conn;
		}
		public void ajoutPari(int id, int idmatch, int coteequipe1, int coteequipe2) throws Exception {
			PreparedStatement pstmt = getConnection().prepareStatement("INSERT INTO Pari VALUES (?,?,?,?,?)");
			pstmt.setInt(1,id);
			pstmt.setInt(2,idmatch);
			pstmt.setInt(3,coteequipe1);
			pstmt.setInt(4, coteequipe2);
			int res=pstmt.executeUpdate();
			System.out.println(res);
		}
		public ObservableList<Pari> recherchePari (int id) throws DAOException {
			ArrayList<Pari> pari = new ArrayList<Pari>();
			ResultSet pariId=null;
			PreparedStatement pstmt3;
			try {
				pstmt3 = getConnection().prepareStatement("SELECT * FROM Pari WHERE id = ?");
				pstmt3.setInt(1,id);
				pariId = pstmt3.executeQuery();
				while(pariId.next()) { 
					Pari pari1 = new Pari(pariId.getInt(1),pariId.getInt(2),pariId.getInt(3), pariId.getInt(4));
					pari.add(pari1);
			}
				pariId.close();
				pstmt3.close();
				ObservableList<Pari> list = FXCollections.observableArrayList();
				list.addAll(pari);
				return list;
				}
			
				catch (SQLException e) {
				DAOException dao = new DAOException ("Erreur SQL",e);
				throw dao;
			}
			catch (ClassNotFoundException e) {
				DAOException dao = new DAOException ("Erreur Driver",e);
				throw dao;
			}
			finally {
				try {
					if (pariId != null) {
						pariId.close();
					}
				}
				catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		public ObservableList<Pari> rechercheAllPari() throws DAOException {
			ArrayList<Pari> p = new ArrayList<Pari>();
			ResultSet resId2=null;
			PreparedStatement pstmt3;
			try {
				pstmt3 = getConnection().prepareStatement("SELECT * FROM Pari");
				resId2 = pstmt3.executeQuery();
				while(resId2.next()) {
					Pari res = new Pari(resId2.getInt(1),resId2.getInt(2),resId2.getInt(3),resId2.getInt(4));
					p.add(res);
				}
				pstmt3.close();
				ObservableList<Pari> list = FXCollections.observableArrayList();
				list.addAll(p);
				return list;

			} catch (SQLException e1) {
				DAOException dao = new DAOException ("Erreur SQL",e1);
				throw dao;
			}
			catch (ClassNotFoundException e1) {
				DAOException dao = new DAOException ("Erreur Driver",e1);
				throw dao;
			}
			finally {
				try {
					if (resId2 != null) {
						resId2.close();
					}
				}
				catch (SQLException e1) {
					e1.printStackTrace();
				}
			}

		}


	}


