package eu.hautil.DAO;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import eu.hautil.joueur.Joueurs;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;


public class JoueursDAO {
	/**
	 * Methode qui permet de se connecter � la base de donn�e
	 * @return
	 * @throws SQLException
	 * @throws ClassNotFoundException
	 */
	public Connection getConnexion() throws SQLException,ClassNotFoundException{
		Connection conn = null;
		String driver = "com.mysql.cj.jdbc.Driver";
		String url = "jdbc:mysql://totodb.c2xrkvuey9hl.eu-west-1.rds.amazonaws.com:3306/ProjetAPIE4?useSSL=false";
		String user="fernat"; 
		String mdp = "thomasf2";
		try{
			Class.forName(driver);
			System.out.println("driver ok");
			conn=DriverManager.getConnection(url,user,mdp);
			System.out.println("connection ok");

		}catch(Exception e){
			e.printStackTrace();
		}
		return conn;
	}
	/**
	 * Methode qui permet d'ajouter un joueur
	 * @param id
	 * @param idequipe
	 * @param poste
	 * @param pseudo
	 * @param ratio
	 * @param nom
	 * @param age
	 * @throws Exception
	 */
	public void ajoutJoueur(int id, int idequipe, String poste, String pseudo, double ratio, String nom, int age) throws Exception {
		PreparedStatement pstmt = getConnexion().prepareStatement("INSERT INTO Joueur VALUES (?,?,?,?,?,?,?)");
		pstmt.setInt(1,id);
		pstmt.setInt(2,idequipe);
		pstmt.setString(3,poste);
		pstmt.setString(4,pseudo);
		pstmt.setDouble(5,ratio);
		pstmt.setString(6,nom);
		pstmt.setInt(7,age);
		int res=pstmt.executeUpdate();
		System.out.println(res);

	}
	/**
	 * Methode qui permet de supprimer un joueur
	 * @throws DAOException
	 */
	public void deleteJoueur() throws DAOException  {
		PreparedStatement pstmt2=null;
		try {
			pstmt2 = getConnexion().prepareStatement("DELETE FROM Joueur WHERE id = 17");
			pstmt2.executeUpdate();
			System.out.println(pstmt2);
		}catch (SQLException e) {
			DAOException dao = new DAOException ("Erreur SQL",e);
			throw dao;
		}
		catch (ClassNotFoundException e) {
			DAOException dao = new DAOException ("Erreur Driver",e);
			throw dao;
		}
		finally {
			try {
				if (pstmt2 != null) {
					pstmt2.close();
				}
			}
			catch (SQLException e) {
				e.printStackTrace();
			}
		}

	}
	/**
	 * Methode qui permet de rechercher un joueur en fonction de son id
	 * @param id
	 * @return
	 * @throws DAOException
	 */
	public Joueurs rechercheJoueur(int id) throws DAOException {
		Joueurs res = null;
		PreparedStatement pstmt3=null;
		try {
			pstmt3 = getConnexion().prepareStatement("SELECT * FROM Joueur WHERE identifiant = ?");
			pstmt3.setInt(1,id);
			ResultSet resId = pstmt3.executeQuery();
			if(resId.next()) 
				res = new Joueurs(resId.getInt(1),resId.getInt(2),resId.getString(3),resId.getString(4),resId.getDouble(5),resId.getString(6),resId.getInt(7));

			resId.close();
			pstmt3.close();
		}catch (SQLException e) {
			DAOException dao = new DAOException ("Erreur SQL",e);
			throw dao;
		}
		catch (ClassNotFoundException e) {
			DAOException dao = new DAOException ("Erreur Driver",e);
			throw dao;
		}
		finally {
			try {
				if (pstmt3 != null) {
					pstmt3.close();
				}
			}
			catch (SQLException e) {
				e.printStackTrace();
			}
		}


		return res;
	}
	/**
	 * Methode qui permet de regarder la liste des joueurs
	 * @return
	 * @throws DAOException
	 */
	public ObservableList<Joueurs> rechercheAllJoueur() throws DAOException {
		ArrayList<Joueurs> j = new ArrayList<Joueurs>();
		ResultSet resId2=null;
		PreparedStatement pstmt3;
		try {
			pstmt3 = getConnexion().prepareStatement("SELECT * FROM Joueur");
			resId2 = pstmt3.executeQuery();
			while(resId2.next()) {
				Joueurs res = new Joueurs(resId2.getInt(1),resId2.getInt(2),resId2.getString(3),resId2.getString(4),resId2.getDouble(5),resId2.getString(6),resId2.getInt(7));
				j.add(res);
			}
			pstmt3.close();
			ObservableList<Joueurs> list = FXCollections.observableArrayList();
			list.addAll(j);
			return list;

		} catch (SQLException e) {
			DAOException dao = new DAOException ("Erreur SQL",e);
			throw dao;
		}
		catch (ClassNotFoundException e) {
			DAOException dao = new DAOException ("Erreur Driver",e);
			throw dao;
		}
		finally {
			try {
				if (resId2 != null) {
					resId2.close();
				}
			}
			catch (SQLException e) {
				e.printStackTrace();
			}
		}

	}

	private PreparedStatement prepareStatement(String reqajout) {
		// TODO Auto-generated method stub
		return null;
	}
}
