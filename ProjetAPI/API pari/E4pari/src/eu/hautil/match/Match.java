package eu.hautil.match;

public class Match {
	int idmatch;
	int idEquipe1;
	int idEquipe2;
	
	public Match(int idmatch , int idEquipe1, int idEquipe2) {
		this.idmatch = idmatch;
		this.idEquipe1=idEquipe1;
		this.idEquipe2=idEquipe2;
	}
	public Match() {
		// TODO Auto-generated constructor stub
	}
	public int getIdMatch() {
		return idmatch;
	}
	public void setIdMatch(int idmatch) {
		this.idmatch = idmatch;
	}
	public int getIdEquipe1() {
		return idEquipe1;
	}
	public void setIdEquipe1(int idEquipe1) {
		this.idEquipe1 = idEquipe1;
	}
	public int getIdEquipe2() {
		return idEquipe2;
	}
	public void setIdEquipe2(int idEquipe2) {
		this.idEquipe2 = idEquipe2;
	}
	public String toString() {
		return "Match = "+idmatch;
	}
}
