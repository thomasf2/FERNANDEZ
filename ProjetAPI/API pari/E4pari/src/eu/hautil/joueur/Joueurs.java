package eu.hautil.joueur;

public class Joueurs {
	int id;
	int idequipe;
	String poste;
	String pseudo;
	double ratio;
	String nom;
	int age;
	/**
	 * Constructeur de base
	 */
	public Joueurs() {
		
	}
	/**
	 * Constructeur d'un joueur
	 * @param id
	 * @param idequipe
	 * @param poste
	 * @param pseudo
	 * @param ratio
	 * @param nom
	 * @param age
	 */
	public Joueurs(int id, int idequipe, String poste, String pseudo, double ratio, String nom, int age) {
		super();
		this.id = id;
		this.idequipe = idequipe;
		this.poste = poste;
		this.pseudo = pseudo;
		this.ratio = ratio;
		this.nom = nom;
		this.age=age;
	}
	/**
	 * r�cup�re l'id du joueur
	 * @return
	 */
	public int getId() {
		return id;
	}
	/**
	 * param�tre l'id du joueur
	 * @param id
	 */
	public void setId(int id) {
		this.id = id;
	}
	/**
	 * r�cup�re l'idequipe du joueur
	 * @return
	 */
	public int getIdequipe() {
		return idequipe;
	}
	/**
	 * param�tre l'idequipe du joueur
	 * @param idequipe
	 */
	public void setIdequipe(int idequipe) {
		this.idequipe = idequipe;
	}
	/**
	 * r�cup�re le poste du joueur
	 * @return
	 */
	public String getPoste() {
		return poste;
	}
	/**
	 * param�tre le poste du joueur
	 * @param poste
	 */
	public void setPoste(String poste) {
		this.poste = poste;
	}
	/**
	 * r�cup�re le pseudo du joueur
	 * @return
	 */
	public String getPseudo() {
		return pseudo;
	}
	/**
	 * param�tre le pseudo du joueur
	 * @param pseudo
	 */
	public void setPseudo(String pseudo) {
		this.pseudo = pseudo;
	}
	/**
	 * r�cup�re le pseudo du joueur
	 * @return
	 */
	public double getRatio() {
		return ratio;
	}
	/**
	 * param�tre le ratio du joueur
	 * @param ratio
	 */
	public void setRatio(double ratio) {
		this.ratio = ratio;
	}
	/**
	 * r�cup�re le nom du joueur
	 * @return
	 */
	public String getNom() {
		return nom;
	}
	/**
	 * param�tre le nom du joueur
	 * @param nom
	 */
	public void setNom(String nom) {
		this.nom = nom;
	}
	/**
	 * r�cup�re l'age du joueur
	 * @return
	 */
	public int getAge() {
		return age;
	}
	/**
	 * param�tre l'age du joueur
	 * @param age
	 */
	public void setAge(int age) {
		this.age = age;
	}
}
	