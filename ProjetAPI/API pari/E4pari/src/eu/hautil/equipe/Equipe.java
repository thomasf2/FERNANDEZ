package eu.hautil.equipe;

public class Equipe {
	int id;
	int idjoueur;
	int cote;
	String club;
	String ratiowin;
	String logo;
	public Equipe () {
		
	}
	public Equipe (int id, int idjoueur ,int cote, String club , String ratiowin , String logo) {
		this.id = id;
		this.cote = cote;
		this.idjoueur = idjoueur;
		this.club = club;
		this.ratiowin = ratiowin;
		this.logo = logo;
	}
	public int getIdEquipe() {
		return id;
	}
	public int getCoteEquipe() {
		return cote;
	}
	public void setCoteEquipe(int cote) {
		this.cote = cote;
	}
	public void setIdEquipe(int id) {
		this.id = id;
	}
	public int getIdJoueur() {
		return idjoueur;
	}
	public void setIdJoueur(int idjoueur) {
		this.idjoueur = idjoueur;
	}
	public String getClub() {
		return club;
	}
	public void setClub(String club) {
		this.club = club;
	}
	public String getRatiowin() {
		return ratiowin;
	}
	public void setRatiowin(String ratiowin) {
		this.ratiowin = ratiowin;
	}
	public String getLogo() {
		return logo;
	}
	public void setLogo(String logo) {
		this.logo=logo;
	}
	public String toString() {
		return "Nom Equipe : "+club;
	}
}
