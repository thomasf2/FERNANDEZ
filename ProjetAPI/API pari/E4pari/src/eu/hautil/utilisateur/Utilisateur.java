package eu.hautil.utilisateur;

public class Utilisateur {
	
	private int id;
	private String nom;
	private String mdp;
	private int age;
	private String email;
	private int fond;
	private String role;
	
	/**
	 * Constructeur de base
	 */
	public Utilisateur(){
		
	}
	/**
	 * Constructeur d'un utilisateur
	 * @param id
	 * @param nom
	 * @param age
	 * @param email
	 * @param fond
	 * @param role
	 */
	public Utilisateur(int id, String nom,String mdp, int age, String email, int fond, String role){
		super();
		this.id = id;
		this.nom = nom;
		this.mdp = mdp; 
		this.age = age;
		this.email = email;
		this.fond = fond;
		this.role = role;
		

	}
	public String getMdp() {
		return mdp;
	}
	public void setMdp(String mdp) {
		this.mdp = mdp;
	}
	/**
	 * r�cup�re l'id de l'utilisateur
	 * @return
	 */
	public int getId() {
		return id;
	}
	/**
	 * 
	 * @param id
	 */
	public void setId(int id) {
		this.id = id;
	}
	/**
	 *r�cup�re le nom de l'utilisateur 
	 * @return
	 */
	public String getNom() {
		return nom;
	}
	/**
	 * param�tre le nom de l'utilisateur
	 * @param nom
	 */
	public void setNom(String nom) {
		this.nom = nom;
	}
	/**
	 * r�cup�re l'age de l'utilisateur
	 * @return
	 */
	public int getAge() {
		return age;
	}
	/**
	 * param�tre l'age de l'utilisateur
	 * @param age
	 */
	public void setAge(int age) {
		this.age = age;
	}
	/**
	 * r�cup�re l'email de l'utilisateur
	 * @return
	 */
	public String getEmail() {
		return email;
	}
	/**
	 * param�tre l'email de l'utilisateur
	 * @param email
	 */
	public void setEmail(String email) {
		this.email = email;
	}
	/**
	 * r�cup�re les fonds de l'utilisateur
	 * @return
	 */
	public int getFond() {
		return fond;
	}
	/**
	 * param�tre les fonds de l'utilisateur
	 * @param fond
	 */
	public void setFond(int fond) {
		this.fond = fond;
	}
	/**
	 * r�cup�re le role de l'utilisateur
	 * @return
	 */
	public String getRole() {
		return role;
	}
	/**
	 *param�tre le role de l'utilisateur 
	 * @param role
	 */
	public void setRole(String role) {
		this.role = role;
	}
}
