package eu.hautil.pari;

public class Pari {
	int id;
	int idmatch;
	int coteequipe1;
	int coteequipe2;
	
	public Pari(int id, int idmatch, int coteequipe1, int coteequipe2) {
		super();
		this.id = id;
		this.idmatch = idmatch;
		this.coteequipe1 = coteequipe1;
		this.coteequipe2 = coteequipe2;
	}
	public Pari() {
		// TODO Auto-generated constructor stub
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getIdmatch() {
		return idmatch;
	}
	public void setIdmatch(int idmatch) {
		this.idmatch = idmatch;
	}
	public int getCoteequipe1() {
		return coteequipe1;
	}
	public void setCoteequipe1(int coteequipe1) {
		this.coteequipe1 = coteequipe1;
	}
	public int getCoteequipe2() {
		return coteequipe2;
	}
	public void setCoteequipe2(int coteequipe2) {
		this.coteequipe2 = coteequipe2;
	}
	public String toString() {
		return "Id Pari ="+ id;
	}
	
}
