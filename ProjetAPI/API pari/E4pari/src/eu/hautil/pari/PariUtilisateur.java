package eu.hautil.pari;

public class PariUtilisateur {
	int id;
	int idutilisateur;
	int fondutilise;
	int idpari;
	public PariUtilisateur(int id, int idutilisateur, int fondutilise, int idpari) {
		super();
		this.id = id;
		this.idutilisateur = idutilisateur;
		this.fondutilise = fondutilise;
		this.idpari = idpari;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getIdutilisateur() {
		return idutilisateur;
	}
	public void setIdutilisateur(int idutilisateur) {
		this.idutilisateur = idutilisateur;
	}
	public int getFondutilise() {
		return fondutilise;
	}
	public void setFondutilise(int fondutilise) {
		this.fondutilise = fondutilise;
	}
	public int getIdpari() {
		return idpari;
	}
	public void setIdpari(int idpari) {
		this.idpari = idpari;
	}
	

}
