<?php
 
class Maison
{
	private $nom;
	private $devise;
	private $armoirie;
	private $datefondation;
	private $region;
	
	
	function __construct($nom,$devise,$armoirie,$datefondation,$region){
		$this->nom=$nom;
		$this->devise=$devise;
		$this->armoirie=$armoirie;
		$this->datefondation=$datefondation;
		$this->region=$region;
	}
	
	public function getNom(){
		return $this->nom;
	}
	public function getDevise(){
		return $this->devise;
	}
	public function getArmoirie(){
		return $this->armoirie;
	}
	public function getDatefondation(){
		return $this->datefondation;
	}
	public function getRegion(){
		return $this->region;
	}
	public function setNom($nom){
		$this->nom=$nom;
	}
	public function setDevise($devise){
		$this->devise=$devise;
	}
	public function setArmoirie($armoirie){
		$this->armoirie=$armoirie;
	}
	public function setDatefondation($datefondation){
		$this->datefondation=$datefondation;
	}
	public function setRegion($region){
		$this->region=$region;
	}
}
?>