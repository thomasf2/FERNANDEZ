<?php
	require_once("characters.php");
    class Noble extends Characters{
      private $epoux_epouse;
      private $pere;
      private $mere;
      private $myHome;
	  
	  
	  function __construct($id,$nom,$dateNaissance,$dateMort,$saCulture,$epoux_epouse,$pere,$mere,$myHome){
		parent::__construct($id,$nom,$dateNaissance,$dateMort,$saCulture);
		$this->epoux_epouse=$epoux_epouse;
		$this->pere=$pere;
		$this->mere=$mere;
		$this->myHome=$myHome;
		
	  }
      public function getEpouxEpouse(){
        return $this->epoux_epouse;
      }

      public function getPere(){
        return $this->pere;
      }

      public function getMere(){
        return $this->mere;
      }

      public function getMyHome(){
        return $this->myHome;
      }

      public function setEpouxEpouse($epoux_epouse){
        $this->epoux_epouse = $epoux_epouse;
      }

      public function setPere($pere){
        $this->pere = $pere;
      }

      public function setMere($mere){
        $this->mere = $mere;
      }

      public function setMyHome($myHome){
        $this->myHome = $myHome;
      }
    }
    ?>
