<?php
	require_once("region.php");
	require_once("classMaison.php");
	require_once("noble.php");
	require_once("characters.php");
	require_once("culture.php");
	$maRegion = new Region(5,"île de fer");
	$maMaison = new Maison("Stark","Ouai la même","Un loup",'2012-06-06',$maRegion);
	$myNoble = new Noble (1,"Bertrand","2011-04-04","Presque mort","blé","Arnaud","Francis","Brigitte",$maMaison);
	$maMaison1 = new Maison("Hutch","Ouai pas trop","Un loup gris",'2012-07-07',$maRegion);
	$maMaison2 = new Maison("Starky","Ouai bof","Un loup vert",'2012-08-08',$maRegion);
	$maCulture = new Culture(7,"vegan");
	$maCulture2 = new Culture(4,"vegetarien");
	$tabnoble = array(1 => $myNoble,
					  2 => new Noble(2,"Bertrander","2011-06-08","Presque pas mort","lin","Aurnad","Fracins","Briguitte",$maMaison),
					  3 => new Noble(3,"Berrander","2012-08-06","On sait pas trop","des trucs","Aurneadad","Fraclins","Brigeuitte",$maMaison),
					  4 => new Noble(4,"Bertrander","2011-06-08","Presque pas mort mais bon","lis","Aurnaedd","Fracinnsns","Briguittye",$maMaison),
					  5 => new Noble(5,"Bererander","2013-12-06","On sait pas tres trop","des trucs pas net","Aureneadad","Franclins","Brigeiuitte",$maMaison),
					  6 => new Noble(6,"Baererander","2005-15-04","On sait pas trop mais osef","des trucs pas net mais cher","Aureeneadad","Fraanclins","Brizgeiuitte",$maMaison)
					);

?>