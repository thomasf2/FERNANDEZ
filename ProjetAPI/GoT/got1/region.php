<?php
    class Region{
		private $id;
		private $lib;
		function __construct($id,$lib){
			$this->id=$id;
			$this->lib=$lib;
		}
		public function getId(){
			return $this->id;
		} 
	    public function getLib(){
			return $this->lib;
        }
		public function setId($id){
			$this->id=$id;
		}
		public function setLib($lib){
			$this->lib=$lib;
		}
    }
	?>
