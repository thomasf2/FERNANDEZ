<?php 
require 'vendor/autoload.php';
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use \Firebase\JWT\JWT;
$app = new \Slim\App;
error_reporting(E_ALL);
ini_set('display_errors', 1);
$app->get('/obtentionToken', function(Request $request, Response $response){  
  $tb = $request->getQueryParams();
  $id = $tb["id"];
  $nom = $tb["nom"];
  $allowed= checkUser($id,$nom);
  if($allowed){
    $token=getTokenJWT();
    return $response->withJson($token,200);

  }else{
    return $response->withStatus(401);
  }
});
$app->post('/monURI', function(Request $request, Response $response){
  $token = $request->getQueryParam('token');
  if(validJWT($token)){
    return validJWT($token);
  }else{
    //non autorisé
    return $response->withStatus(401);
  }  
});

$app->get('/zaza', function(Request $request, Response $response){
  $token = $request->getQueryParam('token');
  if(validJWT($token)){
    return "wazaaaaaaa";
  }else{
    return $response->withStatus(401);
  }	
	
});
$app->get('/bonjour', function(Request $request, Response $response){
  $token = $request->getQueryParam("token");
  if(validJWT($token)){	
	  return "bonjour";
  }else{
    return $response->withStatus(401);
  }
});
$app->get('/nbPersoByLord', function(Request $request, Response $response){
  return getNbPersoByLord();
});
$app->get('/housesOrderByDate', function(Request $request, Response $response){
  return getHouseByDate();
});
$app->get('/personnage/{id}', function(Request $request, Response $response){
$id = $request->getAttribute('id');
return getPersonnage($id);
});
$app->post('/connexion', function(Request $request, Response $response){
   $tb = $request->getQueryParams();
   $token = $tb["token"];
   if(validJWT($token)){
     $id = $tb["id"];
     $nom = $tb["nom"];
     return checkUser($id, $nom);
   }else{
    return $response->withStatus(401);
   }
   
});
$app->post('/insert', function(Request $request, Response $response){
  $tb = $request->getQueryParams();	
  $token = $tb["token"];
  if (validJWT($token)) {
    $id = $tb["id"];  
    $nom = $tb["nom"]; 
    $email = $tb["email"]; 
    return insertUser($id, $nom, $email);
  }else{
    return $response->withStatus(401);
  }
 
});
$app->post('/insertcharac', function(Request $request, Response $response){
  $tb = $request->getQueryParams(); 
  $token = $tb["token"];
  if (validJWT($token)) {
    $id = $tb["id"];  
    $actor = $tb["actor"]; 
    $name = $tb["name"]; 
    return insertCharac($id, $actor, $name);
  }else{
    return $response->withStatus(401);
  }
 
});
$app->post('/delete', function(Request $request, Response $response){
  $tb = $request->getQueryParams();
  $token = $tb["token"];
  if (validJWT($token)) {
    $id = $tb["id"];
    return deleteUser($id);
  }
  else{
    return $response->withStatus(401);
  }
  
});
$app->post('/deletecharac', function(Request $request, Response $response){
  $tb = $request->getQueryParams();
  $token = $tb["token"];
  if (validJWT($token)) {
    $id = $tb["id"];
    return deleteCharac($id);
  }
  else{
    return $response->withStatus(401);
  }
  
});
$app->post('/put',function(Request $request, Response $response){
  $tb = $request->getQueryParams();
  $token = $tb["token"];
  if(validJWT($token)){
    $id = $tb["id"];
    $email = $tb["email"];
    return swapEmail($id, $email);
  }else{
    return $response->withStatus(401);
  }
});
$app->post('/swapperso',function(Request $request, Response $response){
  $tb = $request->getQueryParams();
  $token = $tb["token"];
  if(validJWT($token)){
    $id = $tb["id"];
    $name = $tb["name"];
    return swapPerso($id, $name);
  }else{
    return $response->withStatus(401);
  }
});
$app->get('/personnages',function(Request $request, Response $response){
  $token = $request->getQueryParam("token");
  if(validJWT($token)){
    return tableauperso();
  }else{
    return $response->withStatus(401);
  }
});
$app->get('/nbperso', function(Request $request, Response $response){
  return getPersoByHouses();

});
$app->get('/nbcities', function(Request $request, Response $response){
  return getCitiesByNames();

});
$app->get('/getTrancheperso', function(Request $request, Response $response){
  return trancheperso500();
});
$app->get('/statsCultures', function(Request $request, Response $response){
  return getStatsCultures();
});

function connexion()
{
/*IBM Cloud
* $vcap_services = json_decode($_ENV['VCAP_SERVICES'], true);
* $uri = $vcap_services['compose-for-mysql'][0]['credentials']['uri'];
* $db_creds = parse_url($uri);
* $dbname = "patisserie";
* $dsn = "mysql:host=" . $db_creds['host'] . ";port=" . $db_creds['port'] . ";dbname=" . $dbname;
* return $dbh = new PDO($dsn, $db_creds['user'], $db_creds['pass'],array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'));
* */
//autre
return $dbh = new PDO("mysql:host=totodb.c2xrkvuey9hl.eu-west-1.rds.amazonaws.com;dbname=got4", 'fernat', 'thomasf2', array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'));
}

function checkUser($id, $nom)
{
  $sql = "SELECT email FROM utilisateur WHERE id = '".$id."' AND nom = '".$nom."'";
  try{
    $dbh=connexion();
    $statement = $dbh->prepare($sql);
    $statement->execute();
    $result = $statement->fetchAll(PDO::FETCH_CLASS);  
    foreach ($result as $ligne) {
    	return $ligne->email;
    }
  } catch(PDOException $e){
    return '{"error":'.$e->getMessage().'}';
  }
}

function insertUser($id, $nom, $email)
{
   $sql = "INSERT INTO utilisateur VALUES ('".$id."' , '".$nom."', '".$email."')";
   try{
$dbh=connexion();
$statement = $dbh->prepare($sql);
$statement->execute();
$result = $statement->fetchAll(PDO::FETCH_CLASS); 
return "CA MARCHE PEUT ETRE";
} catch(PDOException $e){
return '{"error":'.$e->getMessage().'}';
}
}
function insertCharac($id, $actor, $name)
{
   $sql = "INSERT INTO characters (id,actor,name) VALUES ('".$id."' , '".$actor."', '".$name."')";
   try{
$dbh=connexion();
$statement = $dbh->prepare($sql);
$statement->execute();
$result = $statement->fetchAll(PDO::FETCH_CLASS); 
return "CA MARCHE PEUT ETRE";
} catch(PDOException $e){
return '{"error":'.$e->getMessage().'}';
}
}

function getPersonnage($id)
{
$sql = "SELECT * FROM characters WHERE id = '".$id."'";
try{
$dbh=connexion();
$statement = $dbh->prepare($sql);
$statement->execute();
$result = $statement->fetchAll(PDO::FETCH_CLASS); 
return json_encode($result, JSON_PRETTY_PRINT);

} catch(PDOException $e){
return '{"error":'.$e->getMessage().'}';
}
}
function getCulture()
{
$sql = "SELECT name FROM cultures";
try{
$dbh=connexion();
$statement = $dbh->prepare($sql);
$statement->execute();
$result = $statement->fetchAll(PDO::FETCH_CLASS); 
return json_encode($result, JSON_PRETTY_PRINT);

} catch(PDOException $e){
return '{"error":'.$e->getMessage().'}';
}
}
function getHouseByDate()
{
$sql = "SELECT houses.name,houses.founded FROM houses WHERE founded IS NOT NULL ORDER BY founded";
try{
$dbh=connexion();
$statement = $dbh->prepare($sql);
$statement->execute();
$result = $statement->fetchAll(PDO::FETCH_CLASS); 
return json_encode($result, JSON_PRETTY_PRINT);

} catch(PDOException $e){
return '{"error":'.$e->getMessage().'}';
}
}
function getNbPersoByLord(){
  $sql = "SELECT COUNT(characters.id) as numper, characters.name as nomper FROM characters, houses WHERE characters.house = houses.id AND current_lord IS NOT NULL GROUP BY current_lord";
  try{
    $dbh=connexion();
    $statement = $dbh->prepare($sql);
    $statement->execute();
    $result = $statement->fetchAll(PDO::FETCH_CLASS);
    return json_encode($result, JSON_PRETTY_PRINT);
  } catch(PDOException $e){
    return '{"error":'.$e->getMessage().'}';

}

}
function deleteUser($id)
{

  try{  
    $sql = "DELETE FROM utilisateur WHERE id = '".$id."'";
    $dbh=connexion();
    $statement = $dbh->prepare($sql);
    $statement->execute();

    return "User supprimer";
  }
  catch(PDOException $e){
    return '{"error":'.$e->getMessage().'}';
  }
}
function deleteCharac($id)
{

  try{  
    $sql = "DELETE FROM characters WHERE id = '".$id."'";
    $dbh=connexion();
    $statement = $dbh->prepare($sql);
    $statement->execute();

    return "Characters supprimer";
  }
  catch(PDOException $e){
    return '{"error":'.$e->getMessage().'}';
  }
}
function swapEmail($id, $email)
{

  try{
    $sql = "UPDATE utilisateur SET email = '".$email."' WHERE id =  '".$id."'";
    $dbh=connexion();
    $statement = $dbh->prepare($sql);
    $statement->execute();
    return "Email Changer";
  }
    catch(PDOException $e){
      return '{"error":'.$e->getMessage().'}';
}
}
function swapPerso($id, $name)
{

  try{
    $sql = "UPDATE characters SET name = '".$name."' WHERE id =  '".$id."'";
    $dbh=connexion();
    $statement = $dbh->prepare($sql);
    $statement->execute();
    return "Nom changé";
  }
    catch(PDOException $e){
      return '{"error":'.$e->getMessage().'}';
}
}
function tableauperso()
{

  try{
    $sql = "SELECT name,titles,culture FROM characters LIMIT 100";
    $dbh=connexion();
    $statement = $dbh->prepare($sql);
    $statement->execute();
    $result = $statement->fetchAll(PDO::FETCH_CLASS); 
    return json_encode($result, JSON_PRETTY_PRINT);    
  }
    catch(PDOException $e){
      return '{"error":'.$e->getMessage().'}';
}
}
function getStatsCultures(){
  $sql = "SELECT COUNT(characters.id) as nb, cultures.name as cult FROM characters, cultures WHERE characters.culture = cultures.id GROUP BY cultures.name;";

  try{
    $dbh=connexion();
    $statement = $dbh->prepare($sql);
    $statement->execute();
    $result = $statement->fetchAll(PDO::FETCH_CLASS);
    return json_encode($result, JSON_PRETTY_PRINT);
  } catch(PDOException $e){
    return '{"error":'.$e->getMessage().'}';

}

}
function trancheperso500(){
  $sql = "call getTranchePerso50";
  try{
    $dbh=connexion();
    $statement = $dbh->prepare($sql);
    $statement->execute();
    $result = $statement->fetchAll(PDO::FETCH_CLASS);
    return json_encode($result, JSON_PRETTY_PRINT);
  } catch(PDOException $e){
    return '{"error":'.$e->getMessage().'}';
  }
}
function getPersoByHouses(){
  $sql = "SELECT houses.name, COUNT(characters.name) as nb FROM characters,houses WHERE houses.id = characters.house GROUP BY houses.name HAVING COUNT(characters.name) >9";
  try{
    $dbh=connexion();
    $statement = $dbh->prepare($sql);
    $statement->execute();
    $result = $statement->fetchAll(PDO::FETCH_CLASS);
    return json_encode($result, JSON_PRETTY_PRINT);
  } catch(PDOException $e){
    return '{"error":'.$e->getMessage().'}';
  }
}
function getCitiesByNames(){
  $sql = "SELECT cities_type.name as name, COUNT(cities.name) as nb FROM cities,cities_type WHERE cities.type = cities_type.id GROUP BY cities_type.name";
  try{
    $dbh=connexion();
    $statement = $dbh->prepare($sql);
    $statement->execute();
    $result = $statement->fetchAll(PDO::FETCH_CLASS);
    return json_encode($result, JSON_PRETTY_PRINT);
  } catch(PDOException $e){
    return '{"error":'.$e->getMessage().'}';
  }
}
function getTokenJWT() {
       // Make an array for the JWT Payload
      $payload = array(
        //30 min
        "exp" => time() + (60 * 30)
      );
       // encode the payload using our secretkey and return the token
      return JWT::encode($payload, "secret");
    }
function validJWT($token) {
      $res = false;
      try {
         $decoded = JWT::decode($token, "secret", array('HS256'));       
        } catch (Exception $e) {
          return $res;
        }
        $res = true;
        return $res;  
      }
       
    $app->run();





  
   
   
  


