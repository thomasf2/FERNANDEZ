var nbPersoByHouse;
var nbPersoByHouseParse;

var nbCityByType;
var nbCityByTypeParse;

var nbPerso;
var nbPersoParse;

  $( document ).ready(function() {
 
  $('#btn-valide').click(function(){ 
    let name=$('#name').val();
      $.ajax({ 
      type: "GET",
      url: "https://sio-hautil.eu/~fernat/got/got8/index.php/personnage/"+name,
      success: function(data){  
        $("#result").html(data);
      }
    });
  });

$('#btn-valid').click(function(){ 
let id=$('#id').val();
let nom=$('#nom').val();
$.ajax({ 
      type: "POST",
      contentType: 'application/json; charset=utf-8',
      url: "https://sio-hautil.eu/~fernat/got/got8/index.php/connexion?id="+id+"&nom="+nom+"&token="+sessionStorage.getItem("token"),
     success: function(data){
         $("#result2").html(data);
      }
 });
});
$('#btn-valide-post').click(function(){ 
let id=$('#id').val();
let nom=$('#nom').val();
let email=$('#email').val();
$.ajax({ 
      type: "POST",
      contentType: 'application/json; charset=utf-8',
      url: "https://sio-hautil.eu/~fernat/got/got8/index.php/insert?id="+id+"&nom="+nom+"&email="+email+"&token="+sessionStorage.getItem("token"),
     success: function(data){
         $("#result3").html(data);
      }
 });
});
$('#btn-valide-postcharac').click(function(){ 
let id=$('#id').val();
let actor=$('#actor').val();
let name=$('#name').val();
$.ajax({ 
      type: "POST",
      contentType: 'application/json; charset=utf-8',
      url: "https://sio-hautil.eu/~fernat/got/got8/index.php/insertcharac?id="+id+"&actor="+actor+"&name="+name+"&token="+sessionStorage.getItem("token"),
     success: function(data){
         $("#result9").html(data);
      }
 });
});
$('#btn-valide-delete').click(function(){
  let id = $('#iden').val();
    $.ajax({
      type: "POST",
      contentType: 'application/json; charset=utf-8',
      url: "https://sio-hautil.eu/~fernat/got/got8/index.php/delete?id="+id+"&token="+sessionStorage.getItem("token"),
      success: function(data){
        $("#result4").html(data);
      }
    });
});
$('#btn-valide-deletecharac').click(function(){
  let id = $('#id').val();
    $.ajax({
      type: "POST",
      contentType: 'application/json; charset=utf-8',
      url: "https://sio-hautil.eu/~fernat/got/got8/index.php/deletecharac?id="+id+"&token="+sessionStorage.getItem("token"),
      success: function(data){
        $("#result4").html(data);
      }
    });
});
$('#btn-valide-swap').click(function(){ 
let id=$('#id').val();
let email=$('#email').val();
$.ajax({ 
      type: "POST",
      contentType: 'application/json; charset=utf-8',
      url: "https://sio-hautil.eu/~fernat/got/got8/index.php/put?id="+id+"&email="+email+"&token="+sessionStorage.getItem("token"),
     success: function(data){
         $("#result5").html(data);
      }
 });
});
$('#btn-valide-swap2').click(function(){ 
let id=$('#id').val();
let name=$('#name').val();
$.ajax({ 
      type: "POST",
      contentType: 'application/json; charset=utf-8',
      url: "https://sio-hautil.eu/~fernat/got/got8/index.php/swapperso?id="+id+"&name="+name+"&token="+sessionStorage.getItem("token"),
     success: function(data){
         $("#result6").html(data);
      }
 });
});
function getRandomColor() {
        var letters = '0123456789ABCDEF';
        var color = '#';
        for (var i = 0; i < 6; i++) {
            color += letters[Math.floor(Math.random() * 16)];
        }
        return color;
    };
  $.ajax({
        type: "GET",
        contentType: 'application/json; charset=utf-8',
        url: "https://sio-hautil.eu/~fernat/got/got8/index.php/nbperso",
        success: function(data){
            nombrePerso = JSON.parse(data);
        }

        });
    $('#btn-stats').click(function(){


        var nb = new Array();
        var maison = new Array();
        var backgroundColor = new Array();
        for (var i = 0; i < nombrePerso.length; i++) {
          nb[i] = nombrePerso[i].nb;
          maison[i] = nombrePerso[i].name;
          backgroundColor[i] = getRandomColor();
        }
        var ctx = document.getElementById("stats").getContext('2d');
        var myChart = new Chart(ctx, {
        type: 'bar',
        data: {
            labels: maison,
            datasets: [{
                label: 'Perso par maison',
                data: nb,
                backgroundColor: backgroundColor
            }]
        },
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero:true
                    }
                }]
            }
        }
    });




    });
     $.ajax({
        type: "GET",
        contentType: 'application/json; charset=utf-8',
        url: "https://sio-hautil.eu/~fernat/got/got8/index.php/nbcities",
        success: function(data){
            nombreCities = JSON.parse(data);
        }

        });
    $('#btn-stats-cities').click(function(){


        var nb = new Array();
        var cities = new Array();
        var backgroundColor = new Array();
        for (var i = 0; i < nombreCities.length; i++) {
          nb[i] = nombreCities[i].nb;
          cities[i] = nombreCities[i].name;
          backgroundColor[i] = getRandomColor();
        }
        var ctx = document.getElementById("stats2").getContext('2d');
        var myChart = new Chart(ctx, {
        type: 'bar',
        data: {
            labels: cities,
            datasets: [{
                label: 'Perso par Cities',
                data: nb,
                backgroundColor: backgroundColor
            }]
        },
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero:true
                    }
                }]
            }
        }
    });
    });
$.ajax({
        type: "GET",
        contentType: 'application/json; charset=utf-8',
        url: "https://sio-hautil.eu/~fernat/got/got8/index.php/statsCultures",
        success: function(data){
            cultures = JSON.parse(data);
        }

        });
    $('#btn-stats-cult').click(function(){

        var nb = new Array();
        var backgroundColor = new Array();
        var nomCulture = new Array();
        for (var i = 0; i < cultures.length; i++) {
          nb[i] = cultures[i].nb;
          nomCulture[i] = cultures[i].cult;
          backgroundColor[i] = getRandomColor();
                  }
        var ctx = document.getElementById("statsCultures").getContext('2d');
        var myChart = new Chart(ctx, {
        type: 'bar',
        data: {
            labels: nomCulture,
            datasets: [{
                label: 'Nombre de perso par culture',
                data: nb,
                backgroundColor: backgroundColor
            }]
        },
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero:true
                    }
                }]
            }
        }
    });
});
$('#btn-valide-users').click(function(){ 

$.ajax({ 
      type: "GET",
      contentType: 'application/json; charset=utf-8',
      url: "https://sio-hautil.eu/~fernat/got/got8/index.php/personnages",
     success: function(data){
         $("#result6").html(data);
      }
 });
});
$("#btn-lord").click(function(event) {
  $("#tableau_utilisateur").bootstrapTable("destroy");
  $("#tableau_utilisateur").bootstrapTable({
url: "https://sio-hautil.eu/~fernat/got/got8/index.php/nbPersoByLord",
columns: [{
field: "numper",
title: 'Culture'
}, {
field: 'nomper',
title: 'Nom'
},
]
});

});
$.ajax({
        type: "GET",
        contentType: 'application/json; charset=utf-8',
        url: "https://sio-hautil.eu/~fernat/got/got8/index.php/nbPersoByLord",
        success: function(data){
            lordName = JSON.parse(data);
        }

        });
    $('#btn-lord').click(function(){

        var nb = new Array();
        var backgroundColor = new Array();
        var nomLord = new Array();
        for (var i = 0; i < lordName.length; i++) {
          nb[i] = lordName[i].numper;
          nomLord[i] = lordName[i].nomper;
          backgroundColor[i] = getRandomColor();
                  }
        var ctx = document.getElementById("stats").getContext('2d');
        var myChart = new Chart(ctx, {
        type: 'bar',
        data: {
            labels: nomLord,
            datasets: [{
                label: 'Nombre de perso par seigneur',
                data: nb,
                backgroundColor: backgroundColor
            }]
        },
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero:true
                    }
                }]
            }
        }
    });
});
$.ajax({
        type: "GET",
        contentType: 'application/json; charset=utf-8',
        url: "https://sio-hautil.eu/~fernat/got/got8/index.php/housesOrderByDate",
        success: function(data){
            housesOrder = JSON.parse(data);
        }

        });
    $('#btn-housesOrder').click(function(){

        var date = new Array();
        var backgroundColor = new Array();
        var nameHouses = new Array();
        for (var i = 0; i < housesOrder.length; i++) {
          date[i] = housesOrder[i].founded;
          nameHouses[i] = housesOrder[i].name;
          backgroundColor[i] = getRandomColor();
                  }
        var ctx = document.getElementById("stats").getContext('2d');
        var myChart = new Chart(ctx, {
        type: 'line',
        data: {
            labels: nameHouses,
            datasets: [{
                label: 'Maison par date de création',
                data: date,
                borderColor: backgroundColor
            }]
        },
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero:true
                    }
                }]
            }
        }
    });
});
$("#btn-affi-culture").click(function(event) {
  $.ajax({
    type:"GET",
    url: "https://sio-hautil.eu/~fernat/got/got8/index.php/cultures?token="+sessionStorage.getItem("token"),
    succes: function(data){
      cultures = JSON.parse(data);
    for(var i =0 ; i<cultures.length() ; i++){
      $('#cultures'.append($('<option>', { 
        value: cultures[i].name,
        text : cultures[i].name 
    })));
    }
    }
  });
});
 $.ajax({
      type: "GET",
      contentType: 'application/json; charset=utf-8',
      url: "https://sio-hautil.eu/~fernat/got/got8/index.php/getTrancheperso",
      success: function(data){
        nbPerso = data;
        nbPersoParse = JSON.parse(data);
      }
    });

    $('#btn-stats-trancheperso').click(function(){
      var nombre = new Array();
      var backgroundColor = new Array();

      for (var i = 0; i < nbPersoParse.length; i++) {
        nombre[i] = nbPersoParse[i].nb;
        backgroundColor[i] = getRandomColor();
      }

      var ctx = document.getElementById("stats2").getContext('2d');
      var myChart = new Chart(ctx, {
          type: 'bar',
          data: {
              labels: ['-100 - 0', '1 - 100', '101 - 200', '201 -> 300'],
              datasets: [{
                  label: 'Nombre de personnage / date de naissance',
                  data: nombre,
                  backgroundColor: backgroundColor
              }]
          },
          options: {
              scales: {
                  yAxes: [{
                      ticks: {
                            beginAtZero:true
                        }
                    }]
                }
            }
        });
    });

     $( "#btn-valide-token").click(function(event) { 
                   $.ajax({ 
                    type: "GET",
                    url: "https://sio-hautil.eu/~fernat/got/got8/index.php/obtentionToken",
                    data: "id="+ $("#id").val()+"&nom="+ $("#nom").val(),
                    success: function(data){
                          sessionStorage.setItem('token', data);    
                          document.location.href="https://sio-hautil.eu/~fernat/got/got8/index.html"                           
                    },
                    error: function(XMLHttpRequest, textStatus, errorThrown) {      
                            $(".form-group-password").addClass("has-danger");
                            $("#password").addClass("form-control-danger");
                    }             
                    });
    });
});
