<?php
	   abstract class Characters{
       protected $id;
       protected $nom;
       protected $dateNaissance;
       protected $dateMort;
       protected $saCulture;
	   static $compteur=0;
	   
	  
		   
	   function __construct($id,$nom,$dateNaissance,$dateMort,$saCulture){
		$this->id=$id;
		$this->nom=$nom;
		$this->dateNaissance=$dateNaissance;
		$this->dateMort=$dateMort;
		$this->saCulture=$saCulture;
		self::$compteur++;
		}		
	  
		   
       public function getId(){
         return $this->id;
       }
       public function getNom(){
         return $this->nom;
       }

       public function getDateNaissance(){
         return $this->dateNaissance;
       }

       public function getDateMort(){
         return $this->dateMort;
       }

       public function getSaCulture(){
         return $this->saCulture;
       }

       public function setId($id){
         $this->id = $id;
       }

       public function setNom($nom){
         $this->nom = $nom;
       }

       public function setDateNaissance($dateNaissance){
         $this->dateNaissance = $dateNaissance;
       }

       public function setDateMort($dateMort){
         $this->dateMort = $dateMort;
       }

       public function setSaCulture($saCulture){
         $this->saCulture = $saCulture;
       }
	   public function __toString(){
		   return "id:".$this->id." nom:".$this->nom." date de naissance : ".$this->dateNaissance." date de mort :".$this->dateMort." culture:".$this->saCulture;
	   }
    }?>
