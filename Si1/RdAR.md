*11/10/2017 #740 SI1 TF*



Aujourd'hui:
-nous avons repondu a des questions types d'evaluations
1. *fred*
2. nom utilisateur+mon machine+~(répertoire courant personnel)+$(début répertoire perso)
3. **w** = permet d'afficher qui est connecte et ce qu'ils sont en train de faire
4. **;** = combine des commandes
5. la personne s'est connecte de maniere expressive
6. **login@** = à quel heure la personne s'est enregistre sur sio-hautil.eu
-nous avons appris quelque commande (cde,wc,cat,ctrl+l,touch)

-nous avons creer un fichier RdAR.md

*18/10/2017 #741 SI1 TF*



Aujourd'hui:
Nous avons fait une mission sur "https://sio-hautil.eu/~fred/p79/SI1/"

-La commande **alias** permet de créer des raccourcis pour faire des commandes plus
 rapidement 
-La commande **type** permet de d'expliquer la commande
-La commande **grep** permet d'afficher tous les mots ayant la même
lettre ou le meme mot que mit dans grep
-La commande **echo** permmet d'afficher un message
-La commande **cat** permet d'afficher le contenu d'un fichier
-La commande **cd** permet de revenir au dossier source
-La commande **pwd** permet de montrer le chemin d'accès
-La commande **chmod** permet de changer le mode de ficher en bits
-La commande **stat** permet d'afficher l'état detaillé d'un fichier ou d'un systeme de fichiers.



*19/10/2017 #741 SI12 TF*



Aujourd'hui:
Nous avons tester les commandes :
-**write** : permet d'écrire a quelqu'un de connecte au meme endroit
-**mesg n/y** : permet d'accepter ou de refuser de recevoir des messages
-**diff** : permet de comparer deux fichier différent
-**w** : affiche les PC connectés et ce qu'ils font
-**.bashrc** : permet de cacher un document
-**cp** : permet de copier un fichier ou un dossier
-**!+n°historique**: tape la commande effectuer anterieurement
-**s** dans man : permet de rechercher un mot dans le manuel
-**ln** : permet de creer un lien physique vers un autre fichier



*08/11/2017 #744 SI1 TF*



Aujourd'hui:
Nous avons créer un fichier avec la commande **ikiwiki --setup /etc/ikiwiki/auto.setup** (Le nom de mon wiki est : **kiwikid**)
Puis j'ai appris quelque commande pour la mise en page du wiki comme : 
-**#+ mot** qui permet de mettre en grand un mot
-****+mot**** qui permet de mettre en gras un mot
-***+mot*** qui permet de mettre en italique un mot
