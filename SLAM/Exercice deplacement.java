class programmeprincip {
	public static void main (String arg []){
		Pixel point1 = new Pixel();
		Pixel point2 = new Pixel(10);
		Pixel point3 = new Pixel(12,75);
		point1.afficher();
		point2.afficher();
		point3.afficher();
		point1 = null;
		point1 = new Pixel(15);
		point1.afficher();
		point1.deplacerx(40);
		point1.afficher();
	}
}





class Pixel {
	int x;
	int y;
	Pixel(int x , int y){
		this.x=x;
		this.y=y;
	}
	Pixel(int valeur){
		x=valeur;
		y=valeur;
	}
	Pixel(){
		x=0;
		y=0;
	}
	void afficher(){
		System.out.println("Pixel :("+x+","+y+")");
	}
	void deplacerx (int dep){
		x=x+dep;
	}
	void deplacery (int dep){
		y=y+dep;
	}
}	
