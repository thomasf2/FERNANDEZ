-- Adminer 4.3.1 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP DATABASE IF EXISTS `fernat`;
CREATE DATABASE `fernat` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `fernat`;

DROP TABLE IF EXISTS `COLLECTION`;
CREATE TABLE `COLLECTION` (
  `numColl` int(11) NOT NULL,
  `nomColl` varchar(60) DEFAULT NULL,
  `dateL` date DEFAULT NULL,
  `harmonie` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`numColl`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `COLLECTION` (`numColl`, `nomColl`, `dateL`, `harmonie`) VALUES
(1,	'Marée Haute',	'2005-04-01',	'Bleu'),
(2,	'Soleil',	'2005-04-15',	'Jaune'),
(3,	'Vent pourpre',	'2011-10-18',	'Rouge');

DROP TABLE IF EXISTS `PRODUIT`;
CREATE TABLE `PRODUIT` (
  `refP` varchar(30) NOT NULL DEFAULT 'B25',
  `design` varchar(60) DEFAULT NULL,
  `coul` varchar(30) DEFAULT NULL,
  `dim` varchar(30) DEFAULT NULL,
  `prixHT` double DEFAULT '65',
  `numColl` int(11) DEFAULT NULL,
  PRIMARY KEY (`refP`),
  KEY `numColl` (`numColl`),
  CONSTRAINT `PRODUIT_ibfk_1` FOREIGN KEY (`numColl`) REFERENCES `COLLECTION` (`numColl`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `PRODUIT` (`refP`, `design`, `coul`, `dim`, `prixHT`, `numColl`) VALUES
('A12',	'Chaise longue',	'Marine',	'120*60',	90,	1),
('A14',	'Serviette de bain',	'Orangé',	'130*80',	65,	2),
('A15',	'Coussin',	'Paille',	'30*30',	12,	2),
('B25',	'Parasol',	'Ciel',	'diam 110',	65,	3),
('B32',	'Parasol',	'Paille',	'diam 90',	45,	3);

-- 2017-09-21 10:06:10
