import java.sql.SQLException;

import java.util.ArrayList;

import eu.hautil.joueur.Joueurs;
import eu.hautil.joueurdao.JoueursDAO;
public class Main {
    public static void main (String[]args) throws SQLException{
        JoueursDAO dao = new JoueursDAO();
        dao.ajoutJoueur();
        
        ArrayList<Joueurs> listejoueur;     
        listejoueur = dao.rechercheAllJoueur();
        for (Joueurs j : listejoueur) {
            System.out.println(j);
        }
        dao.deleteJoueur();
        listejoueur = dao.rechercheAllJoueur();
        for (Joueurs j : listejoueur) {
            System.out.println(j);
        }
    }
}