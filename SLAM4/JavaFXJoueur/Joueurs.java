package eu.hautil.joueur;

import java.sql.Date;

import javafx.scene.control.TextField;

public class Joueurs {
	String nom;
	String prenom;
	String poste;
	String numero;
	String identifiant;
	String club;
	Date datenaiss;
	public Joueurs() {
		
	}
	public Joueurs(String nom, String prenom, String poste, String numero, String identifiant2, String club,Date datenaiss) {
		super();
		this.nom = nom;
		this.prenom = prenom;
		this.poste = poste;
		this.numero = numero;
		this.identifiant = identifiant2;
		this.club = club;
		this.datenaiss=datenaiss;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getPrenom() {
		return prenom;
	}
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}
	public String getPoste() {
		return poste;
	}
	public void setPoste(String poste) {
		this.poste = poste;
	}
	public String getNumero() {
		return numero;
	}
	public void setNumero(String numero) {
		this.numero = numero;
	}
	public String getIdentifiant() {
		return identifiant;
	}
	public void setIdentifiant(String identifiant) {
		this.identifiant = identifiant;
	}
	public String getClub() {
		return club;
	}
	public void setClub(String club) {
		this.club = club;
	}
	@Override
	public String toString() {
		return "identifiant=" + identifiant;
	}
	
}
