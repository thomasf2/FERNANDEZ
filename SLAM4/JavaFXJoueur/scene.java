package DAOtest;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Observable;

import eu.hautil.joueur.Joueurs;
import eu.hautil.joueurdao.*;
import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.stage.Stage;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;

public class scene extends Application {

	public ComboBox <Joueurs> comboBox;
	ObservableList list;
	ImageView iv;

	public static void main(String[] args) {

		Application.launch(scene.class, args);

	}


	@Override

	public void start(Stage primaryStage) throws DAOException, ClassNotFoundException, SQLException {
		primaryStage.setTitle("Application Joueur");
		GridPane grid = new GridPane();
		grid.setAlignment(Pos.CENTER);
		grid.setHgap(10);
		grid.setVgap(10);
		grid.setBackground(new Background(new BackgroundFill(Color.DARKGOLDENROD,CornerRadii.EMPTY,Insets.EMPTY)));
		grid.setPadding(new Insets(50,25,25,25));

		Scene scene = new Scene(grid,500,575);

		Text scenetitle = new Text("Liste des Joueurs");
		scenetitle.setFont(Font.font("Tahoma", FontWeight.NORMAL,20));
		grid.add(scenetitle,1,1,1,1);
		JoueursDAO dao = new JoueursDAO();
		dao.getConnexion();

		Label listeJoueur = new Label("Entrez un identifiant");
		comboBox= new ComboBox <Joueurs>();
		list = dao.rechercheAllJoueur();
		comboBox.getItems().addAll(list); //recupèration de la liste de la combobox et ajout de tous les joueurs -- comboBox.getItems() tout seul = renvoie juste la liste des elements mais pas d'ajout
		grid.add(comboBox,1, 2);






		Button btn = new Button("OK");

		HBox hbBtn = new HBox(10);
		hbBtn.setAlignment(Pos.BOTTOM_RIGHT);
		hbBtn.getChildren().add(btn);
		grid.add(hbBtn, 2, 2);
		Text id = new Text();
		grid.add(id,0, 4);
		Text nom = new Text();
		grid.add(nom,0, 5);
		Text poste = new Text();
		grid.add(poste,0, 6);
		Text club = new Text();
		grid.add(club,0, 7);
		Text prenom = new Text();
		grid.add(prenom,0, 8);
		Text num = new Text();
		grid.add(num,0, 9);




		btn.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent e) {
				String result=comboBox.getPromptText();
				
				Joueurs j;
				String url ="hand-311121_640.png";
				Image image = new Image(url, 100, 100, false, true);
				ImageView iv = new ImageView(image);
				grid.add(iv, 3, 3);
				j = comboBox.getValue(); // récupèration objet joueur selectionné
				System.out.println(j);
				id.setText("Id :"+j.getIdentifiant());
				nom.setText("Nom : "+j.getNom());
				poste.setText("Poste : "+j.getPoste());
				club.setText("Club : "+j.getClub());
				prenom.setText("Prenom : "+j.getPrenom());
				num.setText("Numéro : "+j.getNumero());
				
			}


		}); 



		//textfield1.setText(comboBox.getSelectionModel().getSelectedItem().getIdentifiant());


		primaryStage.setScene(scene);
		primaryStage.show();


	}
}