package eu.hautil.joueurdao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import eu.hautil.joueur.Joueurs;
import javafx.collections.ObservableList;


public class JoueursDAO {
	public Connection getConnexion() throws SQLException,ClassNotFoundException{
		Connection conn = null;
		String driver = "com.mysql.cj.jdbc.Driver";
		String url = "jdbc:mysql://sio-hautil.eu:3306/fernat?user=fernat&password=thomasf2&serverTimezone=UTC";
		String user="fernat"; 
		String mdp = "thomasf2";
		try{
			Class.forName(driver);
			System.out.println("driver ok");
			conn=DriverManager.getConnection(url,user,mdp);
			System.out.println("connection ok");

		}catch(Exception e){
			e.printStackTrace();
		}
		return conn;
	}
	public void ajoutJoueur() throws DAOException {
		PreparedStatement pstmt=null;
		try {
			pstmt = getConnexion().prepareStatement("INSERT INTO JoueurFoot VALUES (?,?,?,?,?,?,?)");
			pstmt.setString(1,"Poutchi");
			pstmt.setString(2,"total");
			pstmt.setString(3,"Milieu");
			pstmt.setInt(4,10);
			pstmt.setString(5,"17");
			pstmt.setString(6,"Bordeaux");
			pstmt.setDate(7, java.sql.Date.valueOf("2002-09-11") );
			int res=pstmt.executeUpdate();
			System.out.println(res);
		} 
		catch (SQLException e) {
			DAOException dao = new DAOException ("Erreur SQL",e);
			throw dao;
		}
		catch (ClassNotFoundException e) {
			DAOException dao = new DAOException ("Erreur Driver",e);
			throw dao;
		}
		finally {
			try {
				if (pstmt != null) {
					pstmt.close();
				}
			}
			catch (SQLException e) {
				e.printStackTrace();
			}
		}

	}
	public void deleteJoueur() throws DAOException  {
		PreparedStatement pstmt2=null;
		try {
			pstmt2 = getConnexion().prepareStatement("DELETE FROM JoueurFoot WHERE identifiant = 17");
			pstmt2.executeUpdate();
			System.out.println(pstmt2);
		} 
		catch (ClassNotFoundException e) {
			DAOException dao = new DAOException ("Erreur Driver",e);
			throw dao;
		} 
		catch (SQLException e) {
			DAOException dao = new DAOException ("Erreur SQL",e);
			throw dao;
		}
		finally {
			try {
				if (pstmt2 != null) {
					pstmt2.close();
				}
			}
			catch (SQLException e) {
				e.printStackTrace();
			}
		}


	}
	public Joueurs rechercheJoueur(String identifiant) throws DAOException {
		Joueurs res = null;
		PreparedStatement pstmt3=null;
		try {
			pstmt3 = getConnexion().prepareStatement("SELECT * FROM JoueurFoot WHERE identifiant = ?");
			pstmt3.setString(1,identifiant);
			ResultSet resId = pstmt3.executeQuery();
			if(resId.next()) 
				res = new Joueurs(resId.getString(1),resId.getString(2),resId.getString(3),resId.getInt(4),identifiant,resId.getString(6),resId.getDate(7));

			resId.close();
			pstmt3.close();
			System.out.println(res);

		} 
		catch (ClassNotFoundException e) {
			DAOException dao = new DAOException ("Erreur Driver",e);
			throw dao;
		} 
		catch (SQLException e) {
			DAOException dao = new DAOException ("Erreur SQL",e);
			throw dao;
		}
		finally {
			try {
				if (pstmt3 != null) {
					pstmt3.close();
				}
			}
			catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return res;
	}
	public ArrayList<Joueurs> rechercheAllJoueur() throws DAOException {
		ArrayList<Joueurs> j = new ArrayList<Joueurs>();

		PreparedStatement pstmt3=null;
		try {
			pstmt3 = getConnexion().prepareStatement("SELECT * FROM JoueurFoot");
			ResultSet resId2 = pstmt3.executeQuery();
			while(resId2.next()) {
				Joueurs res = new Joueurs(resId2.getString(1),resId2.getString(2),resId2.getString(3),resId2.getInt(4),resId2.getString(5),resId2.getString(6),resId2.getDate(7));
				j.add(res);
			} 
		}
		catch (ClassNotFoundException e) {
			DAOException dao = new DAOException ("Erreur Driver",e);
			throw dao;
		} 
		catch (SQLException e) {
			DAOException dao = new DAOException ("Erreur SQL",e);
			throw dao;
		}
		finally {
			try {
				if (pstmt3 != null) {
					pstmt3.close();
				}
			}
			catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return j;
	}


	private PreparedStatement prepareStatement(String reqajout) {
		// TODO Auto-generated method stub
		return null;
	}
}
